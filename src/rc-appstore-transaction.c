/*
 * Copyright © 2016 Collabora Ltd
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rc-appstore-transaction.h"

#include <canterbury/canterbury-platform.h>
#include <ostree.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>

/* Application bundle format */
#define OSTREE_STATIC_DELTA_META_ENTRY_FORMAT "(uayttay)"
#define OSTREE_STATIC_DELTA_FALLBACK_FORMAT "(yaytt)"
#define OSTREE_STATIC_DELTA_SUPERBLOCK_FORMAT "(a{sv}tayay" \
	OSTREE_COMMIT_GVARIANT_STRING "aya" \
	OSTREE_STATIC_DELTA_META_ENTRY_FORMAT \
	"a" OSTREE_STATIC_DELTA_FALLBACK_FORMAT ")"

#include <ribchester/ribchester.h>

#include "mounts.h"
#include "util.h"

/*
 * RcAppStoreTransactionProvider:
 *
 * The implementation of the #RibchesterAppStoreTransaction D-Bus interface.
 * This interface provides monitoring of the Application bundle file
 * installation.
 */

struct _RcAppStoreTransactionProvider
{
  /*< private >*/
  RibchesterAppStoreTransactionSkeleton parent_instance;
  RcBundleManager *bundle_manager;
  gint remote_bundle_fd;
  gboolean released;
  PolkitAuthority *authority;

  gint bundle_fd;
  GVariant *bundle; /* Created in the ostree_load_thread */
  gchar *bundle_path;
  gchar *bundle_id;
  gchar *bundle_version;
  GKeyFile *keyfile; /* Created in the ostree_load_thread */

  GCancellable *cancellable;
  gint stage;
  guint initiator_watch;
};

/* Forward state move */
static void
set_stage (RcAppStoreTransactionProvider *self, gint stage)
{
  g_assert (stage >= self->stage);
  /* The Stage shouldn't be moved forward after going into cancelling or
   * failling */
  g_assert (self->stage >= 0);

  DEBUG ("%p: Starting stage %d", self, stage);
  self->stage = stage;
  ribchester_app_store_transaction_set_stage (RIBCHESTER_APP_STORE_TRANSACTION (self),
                                              stage);
}

static void
unexport_if_possible (RcAppStoreTransactionProvider *self)
{
  gint stage = ribchester_app_store_transaction_get_stage (RIBCHESTER_APP_STORE_TRANSACTION (self));

  /* If we haven't either failed or succeeded yet don't drop from the bus */
  if (stage != RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED &&
      stage != RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLED &&
      stage != RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FAILED)
    return;

  if (!self->released)
    return;

  g_dbus_interface_skeleton_flush (G_DBUS_INTERFACE_SKELETON (self));
  g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self));

  /* Drop the ref we got on ourselves in start */
  g_object_unref (self);
}


static void
unwind_transaction (RcAppStoreTransactionProvider *self)
{
  gint stage = ribchester_app_store_transaction_get_stage (RIBCHESTER_APP_STORE_TRANSACTION (self));

  g_assert (stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLING
            || stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FAILING);

  if (self->stage >= RIBCHESTER_APP_STORE_TRANSACTION_STAGE_UNPACKING &&
      self->stage < RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED)
    {
      g_autofree gchar * ret;
      g_autoptr (GError) error = NULL;

      ret = rc_bundle_manager_roll_back_sync (self->bundle_manager,
                                              self->bundle_id,
                                              &error);
      if (ret == NULL)
        {
           WARNING ("%p: Rollback failed with error: %s (%s, %d)",
                    self,
                    error->message,
                    g_quark_to_string (error->domain), error->code);
        }
    }

  /* If we were cancellign go into cancelled state otherwise */
  if (stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLING)
    ribchester_app_store_transaction_set_stage (RIBCHESTER_APP_STORE_TRANSACTION (self),
                                                RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLED);
  else
    ribchester_app_store_transaction_set_stage (RIBCHESTER_APP_STORE_TRANSACTION (self),
                                                RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FAILED);

  /* Everything else gets cleaned up as part of dispose */
  DEBUG ("%p: Unwinding done", self);
  unexport_if_possible (self);
}

static void
cancel_transaction (RcAppStoreTransactionProvider *self)
{
  gint stage = ribchester_app_store_transaction_get_stage (RIBCHESTER_APP_STORE_TRANSACTION (self));

  DEBUG ("%p: Transaction cancelled", self);

  if (stage < 0)
    return;

  g_cancellable_cancel (self->cancellable);
  ribchester_app_store_transaction_set_stage (RIBCHESTER_APP_STORE_TRANSACTION (self),
                                              RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLING);

  /* If the transaction was already ended we can unwind it straight away
   * otherwise the ongoing async operation will fail with cancelled and
   * fail_transactions takes it from there */
  if (stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED)
    unwind_transaction (self);
}

/* Fail transaction is assumed to be called from the main thread with no
 * async tasks running anymore */
static void
fail_transaction (RcAppStoreTransactionProvider *self, GError *error)
{
  gint stage = ribchester_app_store_transaction_get_stage (RIBCHESTER_APP_STORE_TRANSACTION (self));

  DEBUG ("%p: Transaction failed: stage %d: error: %s (%s, %d)",
           self, stage,
           error->message,
           g_quark_to_string (error->domain),
           error->code);

  if (stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLING)
    {
      /* Assert that the failure is due to the operation being cancelled */
      g_assert (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED));
      unwind_transaction (self);
      return;
    }

  /* Already cancelling or failing */
  if (stage < 0)
    return;

  ribchester_app_store_transaction_set_stage (RIBCHESTER_APP_STORE_TRANSACTION (self),
                                              RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FAILING);

  unwind_transaction (self);
}

static gboolean
handle_cancel (RibchesterAppStoreTransaction *object,
               GDBusMethodInvocation *invocation)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (object);

  cancel_transaction (self);
  ribchester_app_store_transaction_complete_cancel (object, invocation);
  unexport_if_possible (self);

  return TRUE; /* handled */
}

static gboolean
handle_release (RibchesterAppStoreTransaction *object,
                GDBusMethodInvocation *invocation)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (object);

  self->released = TRUE;

  ribchester_app_store_transaction_complete_release (object, invocation);

  unexport_if_possible (self);

  return TRUE; /* handled */
}

static void app_store_transaction_iface_init (RibchesterAppStoreTransactionIface *iface);

G_DEFINE_TYPE_WITH_CODE (RcAppStoreTransactionProvider,
                         rc_app_store_transaction_provider,
                         RIBCHESTER_TYPE_APP_STORE_TRANSACTION_SKELETON,
                         G_IMPLEMENT_INTERFACE (RIBCHESTER_TYPE_APP_STORE_TRANSACTION,
                                                app_store_transaction_iface_init))

static gboolean
authorize (GDBusInterfaceSkeleton *interface,
           GDBusMethodInvocation *invocation,
           gpointer user_data)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (interface);
  g_autoptr (PolkitSubject) subject = NULL;
  g_autoptr (PolkitAuthorizationResult) result = NULL;
  g_autoptr (GError) error = NULL;
  const gchar *sender;

  sender = g_dbus_method_invocation_get_sender (invocation);
  subject = polkit_system_bus_name_new (sender);

  result = polkit_authority_check_authorization_sync (self->authority,
                                                      subject,
                                                      "org.apertis.ribchester.install-bundle",
                                                      NULL,
                                                      POLKIT_CHECK_AUTHORIZATION_FLAGS_ALLOW_USER_INTERACTION,
                                                      NULL,
                                                      &error);

  if (result == NULL)
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "Checking polkit authorization failed: %s",
                                             error->message);
      return FALSE;
    }

  if (!polkit_authorization_result_get_is_authorized (result))
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_ACCESS_DENIED,
                                             "Action not authorized.");
      return FALSE;
    }

  return TRUE;
}

static void
initiator_vanished_cb (GDBusConnection *system_bus,
                       const gchar *name,
                       gpointer user_data)
{
  RcAppStoreTransactionProvider *self = user_data;

  if (!self->released)
    {
      cancel_transaction (self);
      self->released = TRUE;
      unexport_if_possible (self);
    }
}

static void
rc_app_store_transaction_provider_init (RcAppStoreTransactionProvider *self)
{
  self->remote_bundle_fd = -1;
  g_signal_connect (self,
                    "g-authorize-method",
                    G_CALLBACK (authorize),
                    NULL);
}

typedef enum {
  PROP_BUNDLE_MANAGER = 1,
  PROP_AUTHORITY,
  PROP_REMOTE_BUNDLE_FD,
  PROP_INITIATOR,
} Property;

#define NUM_PROPERTIES PROP_INITIATOR + 1

static void
rc_app_store_transaction_provider_get_property (GObject *object,
                                                guint prop_id,
                                                GValue *value,
                                                GParamSpec *pspec)
{
  RcAppStoreTransactionProvider *self = RC_APP_STORE_TRANSACTION_PROVIDER (object);

  switch ((Property) prop_id)
    {
    case PROP_BUNDLE_MANAGER:
      g_value_set_object (value, self->bundle_manager);
      break;
    case PROP_AUTHORITY:
      g_value_set_object (value, self->authority);
      break;
    case PROP_REMOTE_BUNDLE_FD:
      g_value_set_int (value, self->remote_bundle_fd);
      break;

    case PROP_INITIATOR:
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
rc_app_store_transaction_provider_set_property (GObject *object,
                                                guint prop_id,
                                                const GValue *value,
                                                GParamSpec *pspec)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (object);

  switch ((Property) prop_id)
    {
    case PROP_BUNDLE_MANAGER:
      /* construct-only */
      g_assert (self->bundle_manager == NULL);
      self->bundle_manager = g_value_dup_object (value);
      break;
    case PROP_AUTHORITY:
      /* construct-only */
      g_assert (self->authority == NULL);
      self->authority = g_value_dup_object (value);
      break;
    case PROP_REMOTE_BUNDLE_FD:
      /* construct-only */
      g_assert (self->remote_bundle_fd == -1);
      self->remote_bundle_fd = g_value_get_int (value);
      break;
    case PROP_INITIATOR:
      /* construct-only */
        {
          const gchar *initiator;

          g_assert (self->initiator_watch == 0);
          initiator = g_value_get_string (value);

          if (initiator != NULL)
            {
              self->initiator_watch =
                  g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                                    initiator,
                                    G_BUS_NAME_WATCHER_FLAGS_NONE,
                                    NULL,
                                    initiator_vanished_cb,
                                    self, NULL);
            }
        }
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
rc_app_store_transaction_provider_constructed (GObject *object)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (object);

  self->cancellable = g_cancellable_new ();

  G_OBJECT_CLASS (rc_app_store_transaction_provider_parent_class)
      ->constructed (object);
}

static void
rc_app_store_transaction_provider_dispose (GObject *object)
{
  RcAppStoreTransactionProvider *self = RC_APP_STORE_TRANSACTION_PROVIDER (object);

  g_clear_object (&self->bundle_manager);
  g_clear_object (&self->authority);
  g_clear_object (&self->cancellable);

  if (self->initiator_watch != 0)
    {
      g_bus_unwatch_name (self->initiator_watch);
      self->initiator_watch = 0;
    }

  if (self->remote_bundle_fd != -1)
    {
      close (self->remote_bundle_fd);
      self->remote_bundle_fd = -1;
    }

  if (self->bundle_fd != -1)
    {
      close (self->bundle_fd);
      self->bundle_fd = -1;
    }

  if (self->bundle_path != NULL)
    {
      if (g_unlink (self->bundle_path) != 0)
        {
          WARNING ("Unable to unlink \"%s\": %s",
                   self->bundle_path, g_strerror (errno));
        }
      g_clear_pointer (&self->bundle_path, g_free);
    }

  g_clear_pointer (&self->bundle_path, g_free);
  g_clear_pointer (&self->bundle_id, g_free);
  g_clear_pointer (&self->bundle_version, g_free);
  g_clear_pointer (&self->bundle, g_variant_unref);
  g_clear_pointer (&self->keyfile, g_key_file_free);

  G_OBJECT_CLASS (rc_app_store_transaction_provider_parent_class)
      ->dispose (object);
}

static void
rc_app_store_transaction_provider_class_init (RcAppStoreTransactionProviderClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  GParamSpec *property_specs[NUM_PROPERTIES] = { NULL };

  property_specs[PROP_BUNDLE_MANAGER] =
      g_param_spec_object ("bundle-manager", "Bundle manager",
                           "The bundle manager used to implement this interface",
                           RC_TYPE_BUNDLE_MANAGER,
                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  property_specs[PROP_AUTHORITY] =
      g_param_spec_object ("authority", "Polkit Authority",
                           "The Polkit Authority to check permissions against",
                           POLKIT_TYPE_AUTHORITY,
                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  property_specs[PROP_REMOTE_BUNDLE_FD] =
      g_param_spec_int ("remote-bundle-fd", "Remote bundle fd",
                        "Fd for the bundle as passed by the remote caller",
                        -1, G_MAXINT, -1,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  property_specs[PROP_INITIATOR] =
      g_param_spec_string ("initiator", "Initiator",
                           "D-Bus unique name of initiator of transaction",
                           NULL, G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

  object_class->constructed = rc_app_store_transaction_provider_constructed;
  object_class->get_property = rc_app_store_transaction_provider_get_property;
  object_class->set_property = rc_app_store_transaction_provider_set_property;
  object_class->dispose = rc_app_store_transaction_provider_dispose;

  g_object_class_install_properties (object_class,
                                     G_N_ELEMENTS (property_specs),
                                     property_specs);
}

static void
app_store_transaction_iface_init (RibchesterAppStoreTransactionIface *iface)
{
  iface->handle_cancel = handle_cancel;
  iface->handle_release = handle_release;
}

RcAppStoreTransactionProvider *
rc_app_store_transaction_provider_new (RcBundleManager *bundle_manager,
                                       PolkitAuthority *authority,
                                       gint remote_bundle_fd,
                                       const gchar *initiator)
{
  return g_object_new (RC_TYPE_APP_STORE_TRANSACTION_PROVIDER,
                       "bundle-manager", bundle_manager,
                       "authority", authority,
                       "remote-bundle-fd", remote_bundle_fd,
                       "initiator", initiator,
                       NULL);
}

static void
commit_install_cb (GObject *object, GAsyncResult *res, gpointer user_data)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (user_data);
  g_autoptr (GError) error = NULL;

  if (!rc_bundle_manager_commit_install_finish (self->bundle_manager,
                                               res,
                                               &error))
    {
      fail_transaction (self, error);
      return;
    }

  set_stage (self, RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED);
  unexport_if_possible (self);
}

static gboolean
do_unlinkat (RcFileDescriptor fd, const gchar *name, int flags, GError **error)
{
  if (unlinkat (fd, name, flags) < 0)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   g_io_error_from_errno (errno),
                   "Unlink failed for %s: %s",
                   name, g_strerror (errno));
      return FALSE;
    }

  return TRUE;
}


static gboolean
rm_rf_dir (RcFileDescriptor fd, GError **error)
{
  g_autoptr (RcDirectoryIter) d;
  struct dirent *entry;

  d = rc_directory_iter_new (fd, error);
  if (d == NULL)
    return FALSE;

  for (entry = readdir (d) ; entry != NULL; entry = readdir (d))
    {
      if (g_strcmp0 (entry->d_name, ".") == 0
          || g_strcmp0 (entry->d_name, "..") == 0)
        continue;

      if (entry->d_type == DT_DIR)
        {
          g_auto (RcFileDescriptor) dfd = -1;

          dfd = rc_open_dir_at (NULL, fd, entry->d_name,
                                RC_PATH_FLAGS_NOFOLLOW_SYMLINKS, error);
          if (dfd == -1)
            return FALSE;

          if (!rm_rf_dir (dfd, error))
            return FALSE;

          if (!do_unlinkat (fd, entry->d_name, AT_REMOVEDIR, error))
            return FALSE;
        }
      else
        {
          if (!do_unlinkat (fd, entry->d_name, 0, error))
            return FALSE;
        }
    }

  return TRUE;
}

static gboolean
rm_rf_directory (const gchar *path, GError **error)
{
  g_auto (RcFileDescriptor) fd = -1;

  fd = rc_open_dir_at (NULL, AT_FDCWD, path,
                       RC_PATH_FLAGS_NOFOLLOW_SYMLINKS, error);

  if (fd == -1)
    return FALSE;

  if (!rm_rf_dir (fd, error))
    return FALSE;

  if (!do_unlinkat (AT_FDCWD, path, AT_REMOVEDIR, error))
    return FALSE;

  return TRUE;
}

/* Called from a worker thread */
static void
ostree_unpack_thread (GTask *task,
                      gpointer source,
                      gpointer task_data,
                      GCancellable *cancellable)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (source);
  GError *error = NULL;
  g_autoptr (GError) cleanup_error = NULL;
  const gchar *unpack_path = task_data;
  g_autofree gchar * repo_path_str = NULL;
  g_autoptr (GFile) repo_path = NULL;
  g_autoptr (GFile) bundle_file = NULL;
  g_autofree gchar * tmp_repo_path = NULL;
  g_autoptr (GVariant) csum_v = NULL;
  g_autofree gchar *checksum  = NULL;
  OstreeRepoCheckoutAtOptions options = { 0, };
  g_autoptr (OstreeRepo) repo = NULL;
  const gchar *tmp;

  repo_path_str = g_build_filename (unpack_path,
                                    "_repo.XXXXXX",
                                    NULL);

  tmp = g_mkdtemp (repo_path_str);
  if (tmp == NULL)
    {
      g_task_return_new_error (task, G_IO_ERROR,
                               g_io_error_from_errno (errno),
                               "mkdtemp failed for %s: %s",
                               repo_path_str, g_strerror (errno));
      goto out;
    }

  repo_path = g_file_new_for_path (repo_path_str);
  bundle_file = g_file_new_for_path (self->bundle_path);

  repo = ostree_repo_new (repo_path);
  if (!ostree_repo_create (repo, OSTREE_REPO_MODE_BARE_USER,
                           cancellable, &error))
    {
      g_prefix_error (&error, "ostree repo create: ");
      g_task_return_error (task, error);
      goto out;
    }

  if (!ostree_repo_prepare_transaction (repo, NULL, cancellable, &error))
    {
      g_prefix_error (&error, "ostree repo prepare transaction: ");
      g_task_return_error (task, error);
      goto out;
    }

  /* to checksum (ay) */
  csum_v = g_variant_get_child_value (self->bundle, 3);
  if (!ostree_validate_structureof_csum_v (csum_v, &error))
    {
      g_prefix_error (&error, "ostree validate csum: ");
      g_task_return_error (task, error);
      goto out;
    }

  checksum = ostree_checksum_from_bytes_v (csum_v);

  /* Create a master reference for the checksum, this can be anything reallly,
   * just to make it easier to inspect the repository */
  ostree_repo_transaction_set_ref (repo, NULL, "master", checksum);
  if (!ostree_repo_static_delta_execute_offline (repo, bundle_file, FALSE, cancellable, &error))
    {
      g_prefix_error (&error, "ostree delta execute: ");
      g_task_return_error (task, error);
      goto out;
    }

  if (!ostree_repo_commit_transaction (repo, NULL, cancellable, &error))
    {
      g_prefix_error (&error, "ostree commit transaction: ");
      g_task_return_error (task, error);
      goto out;
    }

  options.mode =  OSTREE_REPO_CHECKOUT_MODE_USER;
  options.overwrite_mode =  OSTREE_REPO_CHECKOUT_OVERWRITE_UNION_FILES;
  options.subpath = "/files";
  options.no_copy_fallback = TRUE;

  if (!ostree_repo_checkout_at (repo, &options, AT_FDCWD,
                                unpack_path,
                                checksum, cancellable, &error))
    {
      g_prefix_error (&error, "ostree repo checkout: ");
      g_task_return_error (task, error);
      goto out;
    }

  g_task_return_boolean (task, TRUE);
out:
  /* Clean out the temporary repository leaving the checkout */
  if (!rm_rf_directory (repo_path_str, &cleanup_error))
    {
      WARNING ("%p: Removing repository failed: %s",
               self, cleanup_error->message);
    }
}

static void
ostree_unpack_cb (GObject *object, GAsyncResult *res, gpointer user_data)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (user_data);
  g_autoptr (GError) error = NULL;

  /* Early drop of the cloned bundle file now we're done with its data */
  g_unlink (self->bundle_path);
  g_clear_pointer (&self->bundle_path, g_free);

  if (!g_task_propagate_boolean (G_TASK (res), &error))
    {
      g_object_unref (res);
      fail_transaction (self, error);
      return;
    }
  g_object_unref (res);

  set_stage (self, RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FINISHING);
  rc_bundle_manager_commit_install_async (self->bundle_manager,
                                          self->bundle_id,
                                          self->bundle_version,
                                          self->cancellable,
                                          commit_install_cb,
                                          self);
}

static void
begin_install_cb (GObject *object, GAsyncResult *res, gpointer user_data)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (user_data);
  g_autoptr (GError) error = NULL;
  gchar *unpack_path;
  GTask *task;

  unpack_path = rc_bundle_manager_begin_install_finish (self->bundle_manager,
                                                        res, &error);
  if (unpack_path == NULL)
    {
      fail_transaction (self, error);
      return;
    }

  set_stage (self, RIBCHESTER_APP_STORE_TRANSACTION_STAGE_UNPACKING);
  task = g_task_new (self, self->cancellable, ostree_unpack_cb, self);
  g_task_set_task_data (task, unpack_path, g_free);
  g_task_run_in_thread (task, ostree_unpack_thread);
}

/* Called from a worker thread */
static void
ostree_load_thread (GTask *task,
                    gpointer source,
                    gpointer task_data,
                    GCancellable *cancellable)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (source);
  GError *error = NULL;
  g_autoptr (GMappedFile) map = NULL;
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GVariant) metadata = NULL;
  g_autofree gchar *metadata_keyfile = NULL;

  map = g_mapped_file_new_from_fd (self->bundle_fd, FALSE, &error);
  if (map == NULL)
    {
      g_prefix_error (&error, "Mapping file from fd: ");
      g_task_return_error (task, error);
      return;
    }

  bytes = g_mapped_file_get_bytes (map);
  self->bundle = g_variant_new_from_bytes (G_VARIANT_TYPE(OSTREE_STATIC_DELTA_SUPERBLOCK_FORMAT), bytes, FALSE);
  /* take ownership of the bundle */
  g_variant_ref_sink (self->bundle);

  metadata = g_variant_get_child_value (self->bundle, 0);
  if (!g_variant_lookup (metadata, "metadata", "s", &metadata_keyfile))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                               "No metadata keyfile in bundle superblock");
      return;
    }

  self->keyfile = g_key_file_new ();
  if (!g_key_file_load_from_data (self->keyfile,
                                  metadata_keyfile,
                                  -1, 0,
                                  &error))
    {
      g_prefix_error (&error, "Loading keyfile data: ");
      g_task_return_error (task, error);
    }
  else
    {
      g_task_return_boolean (task, TRUE);
    }
}

static void
ostree_load_cb (GObject *object, GAsyncResult *res, gpointer user_data)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (user_data);
  g_autoptr (GError) error = NULL;

  if (!g_task_propagate_boolean (G_TASK (res), &error))
    {
      g_object_unref (res);
      fail_transaction (self, error);
      return;
    }
  g_object_unref (res);

  self->bundle_id = g_key_file_get_string (self->keyfile,
                                           "Application",
                                           "name",
                                           &error);
  if (self->bundle_id == NULL)
    {
      g_prefix_error (&error, "Failed to find bundle id:");
      fail_transaction (self, error);
      return;
    }

  self->bundle_version = g_key_file_get_string (self->keyfile,
                                                "Application",
                                                "X-Apertis-BundleVersion",
                                                &error);
  if (self->bundle_version == NULL)
    {
      g_prefix_error (&error, "Failed to find bundle version:");
      fail_transaction (self, error);
      return;
    }

  ribchester_app_store_transaction_set_bundle_id (RIBCHESTER_APP_STORE_TRANSACTION (self),
                                                  self->bundle_id);
  ribchester_app_store_transaction_set_bundle_version (RIBCHESTER_APP_STORE_TRANSACTION (self),
                                                       self->bundle_version);

  set_stage (self, RIBCHESTER_APP_STORE_TRANSACTION_STAGE_PREPARING);
  /* TODO use a proper version number */
  rc_bundle_manager_begin_install_async (self->bundle_manager,
                                         self->bundle_id,
                                         self->bundle_version,
                                         self->cancellable,
                                         begin_install_cb,
                                         self);
}

static void
bundle_cloned_cb (GObject *object, GAsyncResult *res, gpointer user_data)
{
  RcAppStoreTransactionProvider *self =
      RC_APP_STORE_TRANSACTION_PROVIDER (user_data);
  g_autoptr (GError) error = NULL;
  GTask *task;

  /* Early close of the remote bundle fd as it's not further needed */
  close (self->remote_bundle_fd);
  self->remote_bundle_fd = -1;

  if (!rc_file_descriptor_clone_content_finish (res, &error))
    {
      fail_transaction (self, error);
      return;
    }

  set_stage (self, RIBCHESTER_APP_STORE_TRANSACTION_STAGE_VERIFYING);

  task = g_task_new (self, self->cancellable, ostree_load_cb, self);
  g_task_run_in_thread (task, ostree_load_thread);
}

void
rc_app_store_transaction_provider_start (RcAppStoreTransactionProvider *self)
{
  g_autoptr (GError) error = NULL;

  DEBUG ("%p: Starting transaction", self);

  /* Take ownership of ourselves, which gets dropped again when unexporting the
   * object */
  g_object_ref (self);

  self->bundle_path =
      g_build_filename (rc_bundle_manager_get_temp_path (self->bundle_manager),
                        "_bundle.XXXXXX", NULL);
  self->bundle_fd = g_mkstemp (self->bundle_path);
  if (self->bundle_fd < 0)
    {
      error = g_error_new (G_IO_ERROR,
                           g_io_error_from_errno (errno),
                           "Creating temporary bundle file failed");
      fail_transaction (self, error);
      return;
    }

  rc_file_descriptor_clone_content_async (self->remote_bundle_fd,
                                          self->bundle_fd,
                                          self->cancellable,
                                          bundle_cloned_cb,
                                          self);
}
