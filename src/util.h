/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __RIBCHESTER_UTIL_H__
#define __RIBCHESTER_UTIL_H__

#include <glib.h>
#include <glib-object.h>

typedef GList RcObjectList;

static inline void
rc_object_list_free (GList *l)
{
  g_list_free_full (l, g_object_unref);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (RcObjectList, rc_object_list_free)

#define ERROR(format, ...) g_error ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define MESSAGE(format, ...) g_message ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define INFO(format, ...) g_info ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

void rc_setenv_disable_services (void);

#endif
