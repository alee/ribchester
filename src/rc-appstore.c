/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rc-appstore.h"

#include <errno.h>
#include <stdio.h>

#include <canterbury/canterbury-platform.h>
#include <glib/gstdio.h>
#include <gio/gunixfdlist.h>

#include <ribchester/ribchester.h>

#include "rc-internal.h"
#include "rc-appstore-transaction.h"
#if ENABLE_PDI
#include "pdi.h"
#endif

/*
 * RcAppStoreProvider:
 *
 * The implementation of the #RibchesterAppStore D-Bus interface.
 * This interface provides installation, upgrades, rollback and removal
 * for store app-bundles.
 */

struct _RcAppStoreProvider
{
  /*< private >*/
  RibchesterAppStoreSkeleton parent_instance;
  RcBundleManager *bundle_manager;
  PolkitAuthority *authority;
};

static void app_store_iface_init (RibchesterAppStoreIface *iface);

G_DEFINE_TYPE_WITH_CODE (RcAppStoreProvider, rc_app_store_provider,
                         RIBCHESTER_TYPE_APP_STORE_SKELETON,
                         G_IMPLEMENT_INTERFACE (RIBCHESTER_TYPE_APP_STORE,
                                                app_store_iface_init))

#if ENABLE_PDI
static Pdi* dbPointer;
static gboolean table_exists (Pdi *dbPointer);

static gboolean set_update_status(RibchesterAppStore *object, GDBusMethodInvocation *invocation, gboolean updateStatus);
static void initialize_rc_db(void);
static void update_db_state (gboolean updateStatus , gboolean bInitialEntry);

#endif

static gboolean remove_app(RibchesterAppStore *object, GDBusMethodInvocation *invocation, const gchar *appName);
static gboolean uninstall_app(RibchesterAppStore *object, GDBusMethodInvocation *invocation, const gchar *appName);
static gboolean reinstall_app(RibchesterAppStore *object, GDBusMethodInvocation *invocation, const gchar *appName);
static gboolean get_boot_status(RibchesterAppStore *object, GDBusMethodInvocation *invocation);

static void
reinstall_done_cb (GObject *object,
    GAsyncResult *result,
    gpointer user_data)
{
  GDBusMethodInvocation *invocation = user_data;
  GError *error = NULL;

  if (g_task_propagate_boolean (G_TASK (result), &error))
    ribchester_app_store_complete_re_install_app (RIBCHESTER_APP_STORE (object),
                                                  invocation, TRUE,
                                                  RIBCHESTER_OK);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);

  g_clear_error (&error);
  g_object_unref (invocation);
}

static void
reinstall_in_thread (GTask *task,
                     gpointer source_object,
                     gpointer task_data,
                     GCancellable *cancellable)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (source_object);
  GError *error = NULL;

  if (rc_bundle_manager_reinstall_sync (self->bundle_manager, task_data,
                                        &error))
    g_task_return_boolean (task, TRUE);
  else
    g_task_return_error (task, error);
}

static gboolean
reinstall_app (RibchesterAppStore *object,
               GDBusMethodInvocation *invocation,
               const gchar *bundle_id)
{
  g_autoptr (GTask) task = g_task_new (object, NULL, reinstall_done_cb,
                                       g_object_ref (invocation));

  g_task_set_task_data (task, g_strdup (bundle_id), g_free);
  g_task_run_in_thread (task, reinstall_in_thread);
  return TRUE;
}

static gboolean
uninstall_app (RibchesterAppStore *object,
               GDBusMethodInvocation *invocation,
               const gchar *bundle_id)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);
  GError *error = NULL;

  if (!rc_bundle_manager_uninstall_sync (self->bundle_manager, bundle_id,
                                         &error))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      g_error_free (error);
    }
  else
    {
      ribchester_app_store_complete_uninstall_app (object, invocation, TRUE,
                                                   RIBCHESTER_OK);
    }

  return TRUE;
}

static gboolean
remove_app (RibchesterAppStore *object,
            GDBusMethodInvocation *invocation,
            const gchar *bundle_id)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);
  GError *error = NULL;

  if (!rc_bundle_manager_remove_sync (self->bundle_manager, bundle_id, &error))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      g_error_free (error);
      return TRUE;
    }

  ribchester_app_store_complete_remove_app (object, invocation, TRUE,
                                            RIBCHESTER_OK);
  return TRUE;
}

static void
rollback_done_cb (GObject *object,
    GAsyncResult *result,
    gpointer user_data)
{
  GDBusMethodInvocation *invocation = user_data;
  GError *error = NULL;
  g_autofree gchar *version = NULL;

  version = g_task_propagate_pointer (G_TASK (result), &error);

  if (version != NULL)
    ribchester_app_store_complete_roll_back (RIBCHESTER_APP_STORE (object),
                                             invocation, version);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);

  g_clear_error (&error);
  g_object_unref (invocation);
}

static void
roll_back_app_in_thread (GTask *task,
                         gpointer source_object,
                         gpointer task_data,
                         GCancellable *cancellable)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (source_object);
  GError *error = NULL;
  g_autofree gchar *what_we_restored = NULL;

  what_we_restored =
    rc_bundle_manager_roll_back_sync (self->bundle_manager,
                                      g_task_get_task_data (task), &error);

  /* takes ownership (in both cases) */
  if (what_we_restored != NULL)
    g_task_return_pointer (task, g_steal_pointer (&what_we_restored), g_free);
  else
    g_task_return_error (task, error);
}

static gboolean
roll_back (RibchesterAppStore *object,
           GDBusMethodInvocation *invocation,
           const gchar *bundle_id)
{
  g_autoptr (GTask) task = NULL;

  g_return_val_if_fail (bundle_id != NULL, FALSE);

  task = g_task_new (object, NULL, rollback_done_cb, g_object_ref (invocation));
  g_task_set_task_data (task, g_strdup (bundle_id), g_free);
  g_task_run_in_thread (task, roll_back_app_in_thread);
  return TRUE;
}

static gboolean
delete_rollback_snapshot (RibchesterAppStore *object,
                          GDBusMethodInvocation *invocation,
                          const gchar *bundle_id)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);
  GError *error = NULL;

  g_return_val_if_fail (bundle_id != NULL, FALSE);

  if (!rc_bundle_manager_delete_rollback_snapshot_sync (self->bundle_manager,
                                                        bundle_id, &error))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      g_error_free (error);
      return TRUE;
    }

  ribchester_app_store_complete_delete_rollback_snapshot (object, invocation);
  return TRUE;
}

static gboolean
rc_app_store_begin_install_cb (RibchesterAppStore *object,
                               GDBusMethodInvocation *invocation,
                               const gchar *bundle_id,
                               const gchar *version,
                               guint flags)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);
  GError *error = NULL;
  g_autofree gchar *temporary_subvolume = NULL;

  if (flags != 0)
    {
      g_set_error (&error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                   "No flags are currently defined");
      goto error;
    }

  temporary_subvolume =
    rc_bundle_manager_begin_install_sync (self->bundle_manager, bundle_id,
                                          version, &error);

  if (temporary_subvolume == NULL)
    goto error;

  ribchester_app_store_complete_begin_install (object, invocation,
                                               temporary_subvolume);
  return TRUE;

error:
  g_dbus_method_invocation_return_gerror (invocation, error);
  g_error_free (error);
  return TRUE;
}


static void
commit_done_cb (GObject *object,
                GAsyncResult *result,
                gpointer user_data)
{
  GDBusMethodInvocation *invocation = user_data;
  RcAppStoreProvider *self =
    RC_APP_STORE_PROVIDER (g_dbus_method_invocation_get_user_data (invocation));
  g_autoptr (GError) error = NULL;

  if (rc_bundle_manager_commit_install_finish (self->bundle_manager,
                                               result,
                                               &error))
    ribchester_app_store_complete_commit_install (RIBCHESTER_APP_STORE (self),
                                                  invocation);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);

  g_object_unref (invocation);
}

static gboolean
rc_app_store_commit_install_cb (RibchesterAppStore *object,
                                GDBusMethodInvocation *invocation,
                                const gchar *bundle_id,
                                const gchar *version)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);

  rc_bundle_manager_commit_install_async (self->bundle_manager,
                                          bundle_id,
                                          version,
                                          NULL,
                                          commit_done_cb,
                                          g_object_ref (invocation));
  return TRUE;
}

static void
rc_app_store_provider_constructed (GObject *object)
{
	RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);

	g_return_if_fail (RC_IS_BUNDLE_MANAGER (self->bundle_manager));

#if ENABLE_PDI
	/*  call this once the startup boot process is complete.
	 * this is to insure proper mounts */
		initialize_rc_db();
#endif

	G_OBJECT_CLASS (rc_app_store_provider_parent_class)->constructed (object);
}

#if ENABLE_PDI
/* Database manager related functions. Needs to be removed once update manager is ready to handle the boot flags */
static void update_db_state (gboolean updateStatus , gboolean bInitialEntry)
{
	if( (NULL != dbPointer) && ( in__pdi_open(dbPointer, "Mount-Manager", "Mount-Manager", SERVICE_DB) == 0) )
	{
		/* Get the state of the flag */
		gint state = updateStatus?1:0;		
		gchar * updateState=g_strdup_printf("%d",state);

		if(TRUE == bInitialEntry)
		{
			GHashTable* hash = g_hash_table_new(g_str_hash, g_str_equal);
			/* Add data to the table */
			g_hash_table_insert(hash, DB_FIELD_NAME, updateState);
			in_PDI_AddData (dbPointer, NULL, hash);
			/* Free unwanted memory */
			g_hash_table_destroy(hash);
		}
		else
		{
			b_PDI_SetPreference(dbPointer, NULL,
					"ID" ,"1",
					DB_FIELD_NAME, updateState);
		}

		if(NULL != updateState)
		{
			g_free(updateState);
			updateState=NULL;
		}
		b_PDI_Close (dbPointer);
	}
}

static gboolean set_update_status(RibchesterAppStore *object, GDBusMethodInvocation *invocation, gboolean updateStatus)
{
	update_db_state (updateStatus , FALSE);

	ribchester_app_store_complete_set_update_status (object, invocation);

	return TRUE;
}


static gboolean table_exists (Pdi *dbPointer)
{
	GPtrArray *gpArray;
	/* Get the array of hash tables */
	gpArray = gp_PDI_GetPreference(dbPointer, NULL, NULL, NULL );
	if(NULL != gpArray)
	{
		RIBCHESTER_TRACE("\n table exists  \n");

		if(gpArray-> len > 0)
		{
			/* Get our data hash table */
			GHashTable *dataHash = g_ptr_array_index(gpArray , 0);
			if(NULL != dataHash)
			{
				/* Get the update status */
				if( NULL != g_hash_table_lookup (dataHash, DB_FIELD_NAME))
					return TRUE;
			}
		}
	}

	return FALSE;
}

static void initialize_rc_db(void)
{
	gint inResult;
	FieldParam_t pfieldparam[1];

	/* Create table for the mount manager */
	dbPointer = _pdi_new();
	if( (NULL != dbPointer) && ( in__pdi_open(dbPointer, "Mount-Manager", "Mount-Manager", SERVICE_DB) == 0) )
	{
		if(FALSE == table_exists (dbPointer))
		{
			/* Set DB field name and type */
			pfieldparam[0].pKeyName = g_strdup(DB_FIELD_NAME);
			pfieldparam[0].inFieldType= SQLITE_TEXT;

			/* Install the table */
			inResult = b_PDI_Install(dbPointer, NULL, pfieldparam, 1 );
			if( !inResult )
			{
				RIBCHESTER_TRACE("\n Table creation Successful ..\n");

				/* Set initial state to FALSE */
				update_db_state (FALSE , TRUE);

				if(NULL != pfieldparam[0].pKeyName)
				{
					g_free(pfieldparam[0].pKeyName);
					pfieldparam[0].pKeyName = NULL;
				}
			}
		}

		b_PDI_Close (dbPointer);
	}
}
#endif




static gboolean get_boot_status(RibchesterAppStore *object, GDBusMethodInvocation *invocation)
{
	gint state = 0;
#if ENABLE_PDI

	if( (NULL != dbPointer) && ( in__pdi_open(dbPointer, "Mount-Manager", "Mount-Manager", SERVICE_DB) == 0) )
	{
		GPtrArray *gpArray;
		/* Get the array of hash tables */
		gpArray = gp_PDI_GetPreference(dbPointer, NULL, NULL, NULL );
		if(NULL != gpArray)
		{
			RIBCHESTER_TRACE("\n table exists  \n");
			/* Get our data hash table */
			GHashTable *dataHash = g_ptr_array_index(gpArray , 0);
			if(NULL != dataHash)
			{
				/* Get the update status */
				gchar *updateState = g_hash_table_lookup (dataHash, DB_FIELD_NAME);
				if(NULL != updateState)
					sscanf (updateState, "%d", &state);
			}
		}

		b_PDI_Close (dbPointer);
	}
#else
	state = rc_read_boot_flags() ;
#endif
	ribchester_app_store_complete_get_boot_status (object, invocation, state);

	return TRUE;
}

static gboolean
install_app_bundle(RibchesterAppStore *object,
                   GDBusMethodInvocation *invocation,
                   GUnixFDList *fd_list,
                   GVariant *bundle)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);
  g_autoptr (RcAppStoreTransactionProvider) transaction = NULL;
  g_autofree gchar *objpath = NULL;
  g_autoptr(GError) error = NULL;
  g_autofree gint *bundle_fds = NULL;
  gint handle = g_variant_get_handle (bundle);
  const gchar *sender;

  fd_list = g_dbus_message_get_unix_fd_list (g_dbus_method_invocation_get_message (invocation));

  /* Only one fd should be passed the one for the bundle handle */
  if (handle > g_unix_fd_list_get_length (fd_list)
      || g_unix_fd_list_get_length (fd_list) != 1)
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_INVALID_ARGS,
                                             "Incorrect fd list");
      return TRUE;
    }

  bundle_fds = g_unix_fd_list_steal_fds (fd_list, NULL);
  sender = g_dbus_method_invocation_get_sender (invocation);
  transaction = rc_app_store_transaction_provider_new (self->bundle_manager,
                                                       self->authority,
                                                       bundle_fds[handle],
                                                       sender);

  objpath = g_strdup_printf ("/org/apertis/Ribchester/AppStoreTransaction_%p",
                             transaction);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (transaction),
                                         g_dbus_method_invocation_get_connection (invocation),
                                         objpath, &error))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return TRUE;
    }

  ribchester_app_store_complete_install_app_bundle (object,
                                                    invocation,
                                                    NULL,
                                                    objpath);
  rc_app_store_transaction_provider_start (transaction);

  return TRUE;
}

static const gchar *install_bundle_methods[] = { "InstallAppBundle", NULL };
static const gchar *manage_bundles_methods[] = { "RemoveApp",
                                                 "UninstallApp",
                                                 "ReInstallApp",
                                                  NULL };
static const gchar *lowlevel_manage_applications_methods[] = { "CreateSubvolume",
                                                               "DeleteRollbackSnapshot",
                                                               "RenameSubvolume",
                                                               "MountSubvolume",
                                                               "UnmountSubvolume",
                                                               "BeginInstall",
                                                               "CommitInstall",
                                                               "RollBack",
                                                               "SetUpdateStatus",
                                                               "GetBootStatus",
                                                               "DeleteSubvolume",
                                                               NULL };


static const struct {
  const gchar *action_id;
  const gchar **methods;
} action_id_mapping[] = {
  { "org.apertis.ribchester.install-bundle",
    install_bundle_methods },
  { "org.apertis.ribchester.manage-applications",
    manage_bundles_methods },
  { "org.apertis.ribchester.lowlevel-manage-applications",
    lowlevel_manage_applications_methods },
};

static const gchar *
rc_app_store_provider_lookup_action_id (const gchar *method)
{
  guint i;

  for (i = 0 ; i < G_N_ELEMENTS (action_id_mapping); i++ )
    {
      if (g_strv_contains (action_id_mapping[i].methods, method))
        return action_id_mapping[i].action_id;
    }

  return NULL;
}


static gboolean
rc_app_store_provider_authorize (GDBusInterfaceSkeleton *interface,
                                 GDBusMethodInvocation *invocation,
                                 gpointer user_data)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (interface);
  g_autoptr (PolkitSubject) subject = NULL;
  g_autoptr (PolkitAuthorizationResult) result = NULL;
  g_autoptr (GError) error = NULL;
  const gchar *sender;
  const gchar *action_id;
  const gchar *method;

  method = g_dbus_method_invocation_get_method_name (invocation);
  action_id = rc_app_store_provider_lookup_action_id (method);
  if (action_id == NULL)
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "Unknown method: %s",
                                             method);
      return FALSE;
    }

  sender = g_dbus_method_invocation_get_sender (invocation);
  subject = polkit_system_bus_name_new (sender);

  result = polkit_authority_check_authorization_sync (self->authority,
    subject,
    action_id,
    NULL,
    POLKIT_CHECK_AUTHORIZATION_FLAGS_ALLOW_USER_INTERACTION,
    NULL,
    &error);

  if (result == NULL)
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "Checking polkit authorization failed: %s",
                                             error->message);
      return FALSE;
    }

  if (!polkit_authorization_result_get_is_authorized (result))
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_ACCESS_DENIED,
                                             "Action not authorized.");
      return FALSE;
    }

  return TRUE;
}

static void
rc_app_store_provider_init (RcAppStoreProvider *self)
{
  g_signal_connect (self, "g-authorize-method",
    G_CALLBACK (rc_app_store_provider_authorize),
    NULL);
}

typedef enum {
    PROP_BUNDLE_MANAGER = 1,
    PROP_AUTHORITY,
} Property;

static void
rc_app_store_provider_get_property (GObject *object,
    guint prop_id,
    GValue *value,
    GParamSpec *pspec)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);

  switch ((Property) prop_id)
    {
      case PROP_BUNDLE_MANAGER:
        g_value_set_object (value, self->bundle_manager);
        break;

      case PROP_AUTHORITY:
        g_value_set_object (value, self->authority);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
rc_app_store_provider_set_property (GObject *object,
    guint prop_id,
    const GValue *value,
    GParamSpec *pspec)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);

  switch ((Property) prop_id)
    {
      case PROP_BUNDLE_MANAGER:
        /* construct-only */
        g_assert (self->bundle_manager == NULL);
        self->bundle_manager = g_value_dup_object (value);
        break;

      case PROP_AUTHORITY:
        /* construct-only */
        g_assert (self->authority == NULL);
        self->authority = g_value_dup_object (value);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
rc_app_store_provider_dispose (GObject *object)
{
  RcAppStoreProvider *self = RC_APP_STORE_PROVIDER (object);

  g_clear_object (&self->bundle_manager);
  g_clear_object (&self->authority);

  G_OBJECT_CLASS (rc_app_store_provider_parent_class)->dispose (object);
}

static void
rc_app_store_provider_class_init (RcAppStoreProviderClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  GParamSpec *property_specs[PROP_AUTHORITY + 1] = { NULL };

  property_specs[PROP_BUNDLE_MANAGER] =
    g_param_spec_object ("bundle-manager", "Bundle manager",
                         "The bundle manager used to implement this interface",
                         RC_TYPE_BUNDLE_MANAGER,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  property_specs[PROP_AUTHORITY] =
    g_param_spec_object ("authority", "Authority",
                         "The Polkit Authority to check permissions against",
                         POLKIT_TYPE_AUTHORITY,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  object_class->constructed = rc_app_store_provider_constructed;
  object_class->get_property = rc_app_store_provider_get_property;
  object_class->set_property = rc_app_store_provider_set_property;
  object_class->dispose = rc_app_store_provider_dispose;

  g_object_class_install_properties (object_class,
                                     G_N_ELEMENTS (property_specs),
                                     property_specs);
}

RcAppStoreProvider *
rc_app_store_provider_new (RcBundleManager *bundle_manager,
                           PolkitAuthority *authority)
{
  return g_object_new (RC_TYPE_APP_STORE_PROVIDER,
                       "bundle-manager", bundle_manager,
                       "authority", authority,
                       NULL);
}

static const gchar *
get_sys_img_dwnld_path_property (RibchesterAppStore *object)
{
  return rc_get_system_image_path ();
}

static const gchar *
get_apps_dwnld_path_property (RibchesterAppStore *object)
{
  return rc_get_apps_dwnld_path ();
}

static void
app_store_iface_init (RibchesterAppStoreIface *iface)
{
  iface->handle_begin_install = rc_app_store_begin_install_cb;
  iface->handle_commit_install = rc_app_store_commit_install_cb;
  iface->handle_remove_app = remove_app;
  iface->handle_uninstall_app = uninstall_app;
  iface->handle_re_install_app = reinstall_app;
  iface->handle_roll_back = roll_back;
  iface->handle_delete_rollback_snapshot = delete_rollback_snapshot;
#if ENABLE_PDI
  iface->handle_set_update_status = set_update_status;
#endif
  iface->handle_get_boot_status = get_boot_status;
  iface->handle_install_app_bundle = install_app_bundle;

  iface->get_sys_img_dwnld_path = get_sys_img_dwnld_path_property;
  iface->get_apps_dwnld_path = get_apps_dwnld_path_property;
}
