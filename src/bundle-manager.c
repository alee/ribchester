/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "bundle-manager.h"

#include <btrfs/ioctl.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <canterbury/canterbury-platform.h>

#include <ribchester/ribchester.h>

#include "fdio.h"
#include "rc-internal.h"

struct _RcBundleManager {
    GObject parent;
    /* GInitable error */
    GError *init_error;

    GDBusConnection *system_bus;

    /* Queue of bundle IDs, highest-priority first */
    GQueue startup_apps;
    gboolean did_startup_apps;

    /* Absolute path to the directory containing all app-bundles */
    gchar *bundles_path;
    /* Absolute path to the directory where the static files of store
     * app-bundles are mounted */
    const gchar *store_mounts_path;
    /* Absolute path to the directory where variable data directories for
     * app-bundles are mounted */
    const gchar *variable_mounts_path;
    /* Absolute path to the directory where per-app-bundle locks are created */
    const gchar *locks_path;

    /* bundles_path opened for reading */
    RcFileDescriptor bundles_fd;
    /* store_mounts_path opened for reading */
    RcFileDescriptor store_mounts_fd;
    /* variable_mounts_path opened for reading */
    RcFileDescriptor variable_mounts_fd;
    /* locks_path opened for reading */
    RcFileDescriptor locks_fd;

    /* Accessed from main thread only */
    struct {
      /* Queue of GTask: attempts to update the component index */
      GQueue tasks;
      /* Subprocess currently updating the component index */
      GSubprocess *subprocess;
    } update_component_index;

    GMutex user_managers_lock;
    /* (element-type utf8 guint)
     * Map from unique name to name watch ID.
     * Protected by user_managers_lock */
    GHashTable *user_managers;
};

static void initable_iface_init (GInitableIface *initable_iface);

G_DEFINE_TYPE_WITH_CODE (RcBundleManager, rc_bundle_manager, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                initable_iface_init))

typedef enum
{
  PROP_SYSTEM_BUS = 1,
} Property;

/* Directory layout:
 *
 * rc_general_rw_mount_point ()
 *     \- app-bundles/
 *         \- org.apertis.Eye/
 *             \- version-1.0/
 *                 \- static+variable/ (subvolume)
 *             \- version-2.0/
 *                 \- static+variable/ (subvolume)
 *             \- rollback -> version-1.0 (symlink)
 *             \- current -> version-2.0 (symlink)
 *         \- com.example.FooBar/
 *             \- version-2.0/
 *                 \- static+variable/ (subvolume)
 *             \- version-2.1/
 *                 \- static+variable/ (subvolume)
 *             \- current -> version-2.0 (symlink)
 *             \- installing -> version-2.1 (symlink)
 *         \- net.example.TemporarilyUninstalled/
 *             \- version-1.2/
 *                 \- static+variable/ (subvolume)
 *             \- uninstalled -> version-1.2 (symlink)
 *
 * The weird name for the subvolume is so we can transition to the
 * planned layout later:
 *
 * rc_general_rw_mount_point ()
 *     \- app-bundles/
 *         \- org.apertis.Eye/
 *             \- version-1.0/
 *                 \- variable/ (subvolume)
 *             \- version-2.0/
 *                 \- variable/ (subvolume)
 *             \- rollback -> version-1.0 (symlink)
 *             \- current -> version-2.0 (symlink)
 *         \- com.example.FooBar/
 *             \- version-2.0/
 *                 \- static/ (subvolume)
 *                 \- variable/ (subvolume)
 *             \- version-2.1/
 *                 \- static/ (subvolume)
 *                 \- variable/ (subvolume)
 *             \- current -> version-2.0 (symlink)
 *             \- installing -> version-2.1 (symlink)
 *         \- net.example.TemporarilyUninstalled/
 *             \- version-1.2/
 *                 \- static/ (subvolume)
 *                 \- variable/ (subvolume)
 *             \- uninstalled -> version-1.2 (symlink)
 */

/* Directory in the general partition below which we store app-bundles */
#define APP_BUNDLES "app-bundles"

/* Subdirectory within a bundle for subvolumes that don't use
 * role -> version symbolic links (no longer used, but we can delete it). */
#define SUBDIR_UNVERSIONED "unversioned"

/* Symlink to a version subdirectory for the current, active version of
 * an app-bundle. */
#define ROLE_CURRENT "current"

/* Symlink to a version subdirectory for the next version of an app-bundle.
 * This is the version we are currently installing: it might not be
 * complete. */
#define ROLE_INSTALLING "installing"

/* Symlink to a version subdirectory for the previous version of an app-bundle.
 * If ROLE_CURRENT turns out to be broken, we'll go back to this one. */
#define ROLE_ROLLBACK "rollback"

/* Symlink to a version subdirectory for the version before ROLE_ROLLBACK.
 * We're going to delete this one, but we keep it around briefly so we can
 * put it back if installation fails. */
#define ROLE_OLD_ROLLBACK "old-rollback"

/* Symlink to a version subdirectory for the version that was recently
 * ROLE_CURRENT, from which we are rolling back.
 * We're going to delete this one, but we keep it around briefly so we can
 * put it back if installation fails. */
#define ROLE_OLD_CURRENT "old-current"

/* Symlink to a version subdirectory for a bundle that has temporarily been
 * uninstalled. ROLE_CURRENT is renamed to this.
 * FIXME: Other app runtimes such as Android don't have this concept, the
 * Applications design doesn't call for it to exist, and it isn't clear how
 * this is meant to interact with other operations on app-bundles
 * (fresh installation, upgrades, rollback). */
#define ROLE_UNINSTALLED "uninstalled"

/* Prefix to put on a version number to get a subdirectory. */
#define VERSION_PREFIX "version-"

/* Version to use if we are asked to create a variable data directory for a
 * built-in app-bundle of an unknown version.
 * FIXME: this shouldn't have to exist; when we mount built-in app-bundles'
 * directories during startup, we should know their versions. */
#define VERSION_UNKNOWN "0"

/* Subvolume within a version subdirectory for the static files and variable
 * data, combined. We don't create this any more, but we retain support
 * for deleting it. */
#define SUBVOL_COMBINED "static+variable"

/* Subvolume within a version subdirectory for the static files only,
 * only used for store app-bundles (built-in app-bundles' static files
 * are in /usr on the main OS filesystem). */
#define SUBVOL_STATIC "static"

/* Subvolume within a version subdirectory for the variable files only. */
#define SUBVOL_VARIABLE "variable"

/*
 * Entirely arbitrary maximum length for version numbers.
 *
 * A 2011 thread on the debian-cd mailing list mentioned
 * a package whose version was
 *
 * 1.0.0~alpha3~git20090817.r1.349dba6-2
 *
 * and even that slightly pathological version number is only 37 bytes,
 * so 64 bytes should be enough for anyone.
 */
#define MAX_VERSION_LENGTH 64

/*
 * Arbitrary maximum length of all the bits of a subvolume name that are
 * not the version number, for instance the "version-" if we use a subvolume
 * named "version-1.2.3".
 */
#define MAX_VERSION_OVERHEAD 64
G_STATIC_ASSERT (sizeof (VERSION_PREFIX) < MAX_VERSION_OVERHEAD);

/*
 * We assert that the maximum version length + overhead are strictly less
 * than the maximum length of a btrfs subvolume, even if we have to add
 * a trailing \0.
 *
 * (In practice, BTRFS_VOL_NAME_MAX is 255, so we have plenty of space.)
 */
G_STATIC_ASSERT (MAX_VERSION_LENGTH + MAX_VERSION_OVERHEAD + 1 <
                 BTRFS_VOL_NAME_MAX);

/*
 * Check that a version number is valid. For now, we only accept the
 * "obvious" version numbers that would be accepted by strverscmp():
 * extensions like Debian-style "-", "~" and "+" syntax could be added later
 * if needed.
 *
 * Because we require it to start with a digit and do not allow slashes
 * anywhere, we are also protected from potentially path-traversing
 * version numbers like "..", "/foo", "foo/../../../bar".
 */
static gboolean
check_valid_version (const gchar *version,
                     GError **error)
{
  const gchar *p;

  if (!g_ascii_isdigit (version[0]))
    {
      g_set_error (error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                   "Version number must start with a digit");
      return FALSE;
    }

  for (p = version; *p != '\0'; p++)
    {
      if (*p != '.' && !g_ascii_isalnum (*p))
        {
          g_set_error (error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                       "Version number must contain only letters, digits and "
                       "dots");
          return FALSE;
        }

      if (p - version > MAX_VERSION_LENGTH)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                       "Version number is too long");
          return FALSE;
        }
    }

  return TRUE;
}

typedef struct
{
  gchar *bundle_id;
  gchar *version;
} InstallData;

static InstallData *
install_data_new (const gchar *bundle_id,
                  const gchar *version)
{
  InstallData *id = g_slice_new0 (InstallData);

  id->bundle_id = g_strdup (bundle_id);
  id->version = g_strdup (version);
  return id;
}

static void
install_data_free (gpointer p)
{
  InstallData *id = p;

  g_free (id->bundle_id);
  g_free (id->version);
  g_slice_free (InstallData, id);
}

/*
 * A #GDestroyNotify for the result of g_bus_watch_name() when stored in a
 * generic data structure using GUINT_TO_POINTER().
 */
static void
name_watch_free (gpointer p)
{
  g_bus_unwatch_name (GPOINTER_TO_UINT (p));
}

static void
rc_bundle_manager_init (RcBundleManager *self)
{
  self->store_mounts_path = CBY_PATH_PREFIX_STORE_BUNDLE;
  self->variable_mounts_path = CBY_PATH_PREFIX_VARIABLE_DATA;
  self->locks_path = "/run/ribchester/app-bundle-locks";
  self->bundles_fd = -1;
  self->store_mounts_fd = -1;
  self->variable_mounts_fd = -1;
  self->locks_fd = -1;
  g_queue_init (&self->startup_apps);
  g_mutex_init (&self->user_managers_lock);
  self->user_managers = g_hash_table_new_full (g_str_hash, g_str_equal,
                                               g_free, name_watch_free);
}

static gboolean
rc_bundle_manager_initable_init (GInitable *initable,
                                 GCancellable *cancellable,
                                 GError **error)
{
  RcBundleManager *self = RC_BUNDLE_MANAGER (initable);

  /* We do the real work in rc_bundle_manager_constructed(), because
   * GInitable's init function is meant to be idempotent. */

  if (self->init_error == NULL)
    return TRUE;

  g_set_error_literal (error, self->init_error->domain, self->init_error->code,
                       self->init_error->message);
  return FALSE;
}

static void
rc_bundle_manager_constructed (GObject *object)
{
  g_auto (RcFileDescriptor) general_fd = -1;
  RcBundleManager *self = RC_BUNDLE_MANAGER (object);

  self->bundles_path = g_build_filename (rc_general_rw_mount_point (),
                                         APP_BUNDLES, NULL);

  /* It should already exist, but create it if necessary */
  if (g_mkdir_with_parents (rc_general_rw_mount_point (), 0755) < 0 &&
      errno != EEXIST)
    {
      g_set_error (&self->init_error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to create \"%s\": %s", rc_general_rw_mount_point (),
                   g_strerror (errno));
      return;
    }

  general_fd = rc_open_dir_at (NULL, AT_FDCWD, rc_general_rw_mount_point (),
                               RC_PATH_FLAGS_NONE, &self->init_error);

  if (general_fd < 0)
    return;

  if (mkdirat (general_fd, APP_BUNDLES, 0755) < 0 && errno != EEXIST)
    {
      g_set_error (&self->init_error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s\": %s", self->bundles_path,
                   g_strerror (errno));
      return;
    }

  if (mkdirat (general_fd, "tmp", 0700) < 0 && errno != EEXIST)
    {
      g_set_error (&self->init_error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s/tmp\": %s",
                   rc_general_rw_mount_point (), g_strerror (errno));
      return;
    }

  self->bundles_fd = rc_open_dir_at (rc_general_rw_mount_point (), general_fd,
                                     APP_BUNDLES, RC_PATH_FLAGS_NONE,
                                     &self->init_error);

  if (self->bundles_fd < 0)
    return;

  if (g_mkdir_with_parents (self->store_mounts_path, 0755) < 0 &&
      errno != EEXIST)
    {
      g_set_error (&self->init_error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s\": %s", self->store_mounts_path,
                   g_strerror (errno));
      return;
    }

  if (g_mkdir_with_parents (self->variable_mounts_path, 0755) < 0 &&
      errno != EEXIST)
    {
      g_set_error (&self->init_error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s\": %s", self->variable_mounts_path,
                   g_strerror (errno));
      return;
    }

  self->store_mounts_fd =
    rc_open_dir_at (NULL, AT_FDCWD, self->store_mounts_path, RC_PATH_FLAGS_NONE,
                    &self->init_error);

  if (self->store_mounts_fd < 0)
    return;

  self->variable_mounts_fd =
    rc_open_dir_at (NULL, AT_FDCWD, self->variable_mounts_path,
                    RC_PATH_FLAGS_NONE, &self->init_error);

  if (self->variable_mounts_fd < 0)
    return;

  if (g_mkdir_with_parents (self->locks_path, 0700) < 0 && errno != EEXIST)
    {
      g_set_error (&self->init_error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s\": %s", self->locks_path,
                   g_strerror (errno));
      return;
    }

  self->locks_fd = rc_open_dir_at (NULL, AT_FDCWD, self->locks_path,
                                   RC_PATH_FLAGS_NONE, &self->init_error);

  if (self->locks_fd < 0)
    return;
}

static void
rc_bundle_manager_get_property (GObject *object,
                                guint prop_id,
                                GValue *value,
                                GParamSpec *pspec)
{
  RcBundleManager *self = RC_BUNDLE_MANAGER (object);

  switch ((Property) prop_id)
    {
      case PROP_SYSTEM_BUS:
        g_value_set_object (value, self->system_bus);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
rc_bundle_manager_set_property (GObject *object,
                                guint prop_id,
                                const GValue *value,
                                GParamSpec *pspec)
{
  RcBundleManager *self = RC_BUNDLE_MANAGER (object);

  switch ((Property) prop_id)
    {
      case PROP_SYSTEM_BUS:
        /* construct-only */
        g_assert (self->system_bus == NULL);
        self->system_bus = g_value_dup_object (value);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
rc_bundle_manager_dispose (GObject *object)
{
  RcBundleManager *self = RC_BUNDLE_MANAGER (object);

  {
    g_autoptr (GMutexLocker) lock =
        g_mutex_locker_new (&self->user_managers_lock);
    g_clear_pointer (&self->user_managers, g_hash_table_unref);
  }

  g_clear_object (&self->system_bus);

  G_OBJECT_CLASS (rc_bundle_manager_parent_class)->dispose (object);
}

static void
rc_bundle_manager_finalize (GObject *object)
{
  RcBundleManager *self = RC_BUNDLE_MANAGER (object);

  g_mutex_clear (&self->user_managers_lock);
  g_free (self->bundles_path);

  if (self->bundles_fd >= 0)
    g_close (self->bundles_fd, NULL);

  if (self->store_mounts_fd >= 0)
    g_close (self->store_mounts_fd, NULL);

  if (self->variable_mounts_fd >= 0)
    g_close (self->variable_mounts_fd, NULL);

  G_OBJECT_CLASS (rc_bundle_manager_parent_class)->finalize (object);
}

static void
rc_bundle_manager_class_init (RcBundleManagerClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  GParamSpec *property_specs[PROP_SYSTEM_BUS + 1] = { NULL };

  object_class->constructed = rc_bundle_manager_constructed;
  object_class->get_property = rc_bundle_manager_get_property;
  object_class->set_property = rc_bundle_manager_set_property;
  object_class->dispose = rc_bundle_manager_dispose;
  object_class->finalize = rc_bundle_manager_finalize;

  property_specs[PROP_SYSTEM_BUS] =
      g_param_spec_object ("system-bus", "System bus",
                           "A connection to the D-Bus system bus",
                           G_TYPE_DBUS_CONNECTION,
                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  g_object_class_install_properties (object_class,
                                     G_N_ELEMENTS (property_specs),
                                     property_specs);
}

RcBundleManager *
rc_bundle_manager_new (GDBusConnection *system_bus,
                       GError **error)
{
  g_return_val_if_fail (G_IS_DBUS_CONNECTION (system_bus), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  return g_initable_new (RC_TYPE_BUNDLE_MANAGER, NULL, error,
                         "system-bus", system_bus,
                         NULL);
}

static void
initable_iface_init (GInitableIface *initable_iface)
{
  initable_iface->init = rc_bundle_manager_initable_init;
}

/*
 * OpenFlags:
 * @OPEN_NONE: no special behaviour
 * @OPEN_CREATE: if the app-bundle or version doesn't exist, create it instead
 *  of raising %RC_ERROR_NOT_FOUND
 * @OPEN_INCOMPLETE: we are still in the process of installing this version,
 *  so it is not an error for the variable data subvolume to be missing
 *
 * Flags affecting how we open an app-bundle or version.
 */
typedef enum
{
  OPEN_NONE = 0,
  OPEN_CREATE = (1 << 0),
  OPEN_INCOMPLETE = (1 << 1),
} OpenFlags;

/*
 * RcBundle:
 * @path: (nullable): the absolute path to the app-bundle's storage location,
 *  or %NULL if closed
 * @variable_mount_point: (nullable): where we expect the variable part of
 *  this bundle to be mounted, or %NULL if closed
 * @store_mount_point: (nullable): where we expect the static part of this
 *  bundle to be mounted, or %NULL if closed or a built-in app-bundle
 * @fd: opened file descriptor at @path, or -1 if closed
 * @lock: opened file descriptor on the corresponding lock file, used to
 *  protect this app-bundle from concurrent modification via a
 *  different #RcBundle (maybe in a different thread), or even a different
 *  process that participates in the same advisory locking scheme
 *
 * Data structure representing an opened app-bundle.
 */
typedef struct
{
  gchar *path;
  gchar *variable_mount_point;
  gchar *store_mount_point;
  CbyProcessType type;
  RcFileDescriptor fd;
  RcFileDescriptor lock;
} RcBundle;

/*
 * RC_BUNDLE_INIT:
 *
 * A static initializer for an RcBundle.
 */
#define RC_BUNDLE_INIT { \
  .path = NULL, \
  .variable_mount_point = NULL, \
  .store_mount_point = NULL, \
  .type = CBY_PROCESS_TYPE_UNKNOWN,  \
  .fd = -1, \
  .lock = -1, \
}

/*
 * rc_bundle_init:
 * @self: (out caller-allocates): a bundle structure to be overwritten;
 *  must either be uninitialized or already initialized to %RC_BUNDLE_INIT
 *
 * Initialize @self to %RC_BUNDLE_INIT, without freeing any existing
 * contents.
 */
static void
rc_bundle_init (RcBundle *self)
{
  self->path = NULL;
  self->variable_mount_point = NULL;
  self->store_mount_point = NULL;
  self->type = CBY_PROCESS_TYPE_UNKNOWN;
  self->fd = -1;
  self->lock = -1;
}

/*
 * rc_bundle_clear:
 * @self: a bundle structure, which must already have been initialized
 *  or cleared
 *
 * Close the bundle and set its members to the same values as %RC_BUNDLE_INIT.
 */
static void
rc_bundle_clear (RcBundle *self)
{
  g_free (self->path);
  g_free (self->variable_mount_point);
  g_free (self->store_mount_point);

  if (self->fd >= 0)
    g_close (self->fd, NULL);

  if (self->lock >= 0)
    g_close (self->lock, NULL);

  rc_bundle_init (self);
}

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (RcBundle, rc_bundle_clear);

/*
 * rc_bundle_is_open:
 * @self: a bundle structure, which must already have been initialized
 *  or cleared
 *
 * Returns: %TRUE if the bundle is considered to be "open".
 */
static gboolean
rc_bundle_is_open (RcBundle *self)
{
  if (self->path == NULL)
    {
      g_assert (self->variable_mount_point == NULL);
      g_assert (self->store_mount_point == NULL);
      g_assert (self->type == CBY_PROCESS_TYPE_UNKNOWN);
      g_assert (self->fd < 0);
      g_assert (self->lock < 0);
      return FALSE;
    }
  else
    {
      g_assert (self->variable_mount_point != NULL);
      g_assert (self->fd >= 0);
      g_assert (self->lock >= 0);

      switch (self->type)
        {
        case CBY_PROCESS_TYPE_STORE_BUNDLE:
          g_assert (self->store_mount_point != NULL);
          break;

        case CBY_PROCESS_TYPE_BUILT_IN_BUNDLE:
          g_assert (self->store_mount_point == NULL);
          break;

        case CBY_PROCESS_TYPE_UNKNOWN:
        case CBY_PROCESS_TYPE_PLATFORM:
        default:
          g_assert_not_reached ();
        }

      return TRUE;
    }
}

/*
 * rc_bundle_steal_from:
 * @dest: (out caller-allocates): a bundle structure to be overwritten;
 *  must either be uninitialized or initialized to %RC_BUNDLE_INIT on entry
 * @source: a bundle to move to @dest
 *
 * Move the contents of @source to @dest, and clear @source.
 * Analogous to `dest = g_steal_pointer (&source)` or
 * `dest = rc_file_descriptor_steal (&source)`.
 */
static void
rc_bundle_steal_from (RcBundle *dest,
                      RcBundle *source)
{
  *dest = *source;
  rc_bundle_init (source);
}

/*
 * RcBundleVersion:
 * @version: (nullable): the version number of the version, or %NULL if closed
 * @directory: (nullable): the basename of the version's storage location,
 *  or %NULL if closed
 * @path: (nullable): the absolute path to the version's storage location,
 *  or %NULL if closed
 * @variable_subvol_path: (nullable): the absolute path to the subvolume
 *  within @path containing variable data, or %NULL if closed
 * @store_subvol_path: (nullable): the absolute path to the subvolume
 *  within @path containing store app-bundles' static data, or %NULL if closed
 *  or not a store app-bundle
 * @fd: opened file descriptor at @path, or -1 if closed
 * @variable_subvol_fd: opened file descriptor at @variable_subvol_path,
 *  or -1 if closed
 * @store_subvol_fd: opened file descriptor at @store_subvol_path,
 *  or -1 if closed
 *
 * Data structure representing a particular version subdirectory in an
 * app-bundle.
 */
typedef struct
{
  /* @version points into @path */
  const gchar *version;
  /* @directory points into @path */
  const gchar *directory;
  gchar *path;
  gchar *variable_subvol_path;
  gchar *store_subvol_path;
  RcFileDescriptor fd;
  RcFileDescriptor variable_subvol_fd;
  RcFileDescriptor store_subvol_fd;
} RcBundleVersion;

/*
 * RC_BUNDLE_INIT:
 *
 * A static initializer for an RcBundleVersion.
 */
#define RC_BUNDLE_VERSION_INIT \
{ \
  .version = NULL, \
  .directory = NULL, \
  .path = NULL, \
  .variable_subvol_path = NULL, \
  .store_subvol_path = NULL, \
  .fd = -1, \
  .variable_subvol_fd = -1, \
  .store_subvol_fd = -1, \
}

/*
 * rc_bundle_version_init:
 * @self: (out caller-allocates): a version structure to be overwritten;
 *  must either be uninitialized or already initialized
 *  to %RC_BUNDLE_VERSION_INIT
 *
 * Initialize @self to %RC_BUNDLE_VERSION_INIT, without freeing any existing
 * contents.
 */
static void
rc_bundle_version_init (RcBundleVersion *self)
{
  self->version = NULL;
  self->directory = NULL;
  self->path = NULL;
  self->variable_subvol_path = NULL;
  self->store_subvol_path = NULL;
  self->fd = -1;
  self->variable_subvol_fd = -1;
  self->store_subvol_fd = -1;
}

/*
 * rc_bundle_version_clear:
 * @self: a version structure, which must already have been initialized
 *  or cleared
 *
 * Close the bundle and set its members to the same values as
 * %RC_BUNDLE_VERSION_INIT.
 */
static void
rc_bundle_version_clear (RcBundleVersion *self)
{
  g_free (self->path);
  g_free (self->variable_subvol_path);
  g_free (self->store_subvol_path);

  if (self->fd >= 0)
    g_close (self->fd, NULL);

  if (self->variable_subvol_fd >= 0)
    g_close (self->variable_subvol_fd, NULL);

  if (self->store_subvol_fd >= 0)
    g_close (self->store_subvol_fd, NULL);

  rc_bundle_version_init (self);
}

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (RcBundleVersion, rc_bundle_version_clear);

/*
 * rc_bundle_version_is_open:
 * @self: a bundle version structure, which must already have been initialized
 *  or cleared
 *
 * Returns: %TRUE if the version is considered to be "open".
 */
static gboolean
rc_bundle_version_is_open (RcBundleVersion *self)
{
  if (self->path == NULL)
    {
      g_assert (self->version == NULL);
      g_assert (self->directory == NULL);
      g_assert (self->path == NULL);
      g_assert (self->variable_subvol_path == NULL);
      g_assert (self->store_subvol_path == NULL);
      g_assert (self->fd < 0);
      g_assert (self->variable_subvol_fd < 0);
      g_assert (self->store_subvol_fd < 0);
      return FALSE;
    }
  else
    {
      g_assert (self->version != NULL);
      g_assert (self->directory != NULL);
      g_assert (self->path != NULL);
      g_assert (self->variable_subvol_path != NULL);
      g_assert (self->fd >= 0);

      /* variable_subvol_fd may be negative if opened with
       * OPEN_INCOMPLETE - that's OK, so we don't check it. */

      if (self->store_subvol_path != NULL)
        g_assert (self->store_subvol_fd >= 0);
      else
        g_assert (self->store_subvol_fd < 0);

      return TRUE;
    }
}

/*
 * rc_bundle_version_steal_from:
 * @dest: (out caller-allocates): a version structure to be overwritten;
 *  must either be uninitialized or initialized to %RC_BUNDLE_VERSION_INIT
 *  on entry
 * @source: a version to move to @dest
 *
 * Move the contents of @source to @dest, and clear @source.
 * Analogous to `dest = g_steal_pointer (&source)` or
 * `dest = rc_file_descriptor_steal (&source)`.
 */
static void
rc_bundle_version_steal_from (RcBundleVersion *dest,
                              RcBundleVersion *source)
{
  *dest = *source;
  rc_bundle_version_init (source);
}

typedef struct
{
  RcBundleManager *self;
  const gchar *bundle_id;
  gsize pending_terminations;
  GError *first_error;
} TerminateBundleState;

static void
terminate_bundle_cb (GObject *source_object,
                     GAsyncResult *result,
                     gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) variant = NULL;
  TerminateBundleState *state = user_data;

  variant = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                           result, &error);

  if (variant == NULL)
    {
      if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        {
          DEBUG ("TerminateBundle(\"%s\") cancelled, ignoring",
                 state->bundle_id);
        }
      else if (g_error_matches (error, G_DBUS_ERROR,
                                G_DBUS_ERROR_NAME_HAS_NO_OWNER))
        {
          /* The Canterbury instance went away while we were waiting for it
           * to terminate an app-bundle. This might be a race condition
           * between sending the method call (in a worker thread), and
           * removing its registration as a per-user app-manager (in the
           * main thread). We can only assume that this is non-problematic... */
          DEBUG ("TerminateBundle(\"%s\") raised NameHasNoOwner; ignoring",
                 state->bundle_id);
          g_clear_error (&error);
        }
      else
        {
          WARNING ("TerminateBundle(\"%s\") failed: %s", state->bundle_id,
                   error->message);

          if (state->first_error == NULL)
            state->first_error = g_steal_pointer (&error);
        }
    }

  g_assert (state->pending_terminations >= 1);
  state->pending_terminations--;
}

/*
 * rc_bundle_manager_prerm_sync:
 * @self: the bundle manager
 * @bundle_id: the bundle ID
 * @bundle: the open and locked bundle
 * @error: used to raise a %RC_ERROR on failure
 *
 * Perform pre-removal actions for @bundle. Like dpkg's prerm maintainer
 * script, this is called before removal, upgrade or rollback.
 */
static gboolean
rc_bundle_manager_prerm_sync (RcBundleManager *self,
                              const gchar *bundle_id,
                              RcBundle *bundle,
                              GCancellable *cancellable,
                              GError **error)
{
  g_autoptr (GMainContext) main_context = g_main_context_new ();
  g_autoptr (GSubprocess) prerm = NULL;
  g_autofree gchar *program = NULL;
  TerminateBundleState state = { self, bundle_id, 0, NULL };

  g_main_context_push_thread_default (main_context);

  {
    g_autoptr (GMutexLocker) lock =
        g_mutex_locker_new (&self->user_managers_lock);
    GHashTableIter iter;
    gpointer key;

    g_hash_table_iter_init (&iter, self->user_managers);

    while (g_hash_table_iter_next (&iter, &key, NULL))
      {
        const gchar *unique_name = key;

        state.pending_terminations++;
        DEBUG ("Asking %s to terminate %s", unique_name, bundle_id);
        /* We wait as long as systemd will wait between sending SIGTERM
         * and SIGKILL (CBY_SERVICE_STOP_TIMEOUT_SEC), plus an arbitrary
         * 5 seconds to allow for the D-Bus messages and for the SIGKILL
         * signal to take effect. */
        g_dbus_connection_call (self->system_bus, unique_name,
                                CBY_OBJECT_PATH_PER_USER_APP_MANAGER1,
                                CBY_INTERFACE_PER_USER_APP_MANAGER1,
                                "TerminateBundle",
                                g_variant_new ("(s)", bundle_id),
                                G_VARIANT_TYPE_UNIT,
                                G_DBUS_CALL_FLAGS_NONE,
                                (CBY_SERVICE_STOP_TIMEOUT_SEC + 5) * 1000,
                                cancellable, terminate_bundle_cb, &state);
      }
  }

  while (state.pending_terminations > 0)
    g_main_context_iteration (main_context, TRUE);

  g_main_context_pop_thread_default (main_context);

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  /* TODO: If a Canterbury instance fails, should we carry on regardless,
   * or terminate the maintenance operation, or does it depend which
   * maintenance operation we're doing? */
  if (state.first_error != NULL)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to terminate programs in \"%s\": %s",
                   bundle_id, state.first_error->message);
      g_error_free (state.first_error);
      return FALSE;
    }

  program = g_find_program_in_path ("canterbury-prerm");

  if (program == NULL)
    {
      INFO ("Not running canterbury-prerm: not found");
      return TRUE;
    }

  DEBUG ("Running canterbury-prerm \"%s\"...", bundle_id);

  /* We run a subprocess (provided by Canterbury) so that we can transition
   * to a more restrictive AppArmor profile, rather than parsing app-supplied
   * data in the highly-privileged Ribchester process. */
  prerm = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                            error,
                            "canterbury-prerm",
                            bundle_id,
                            NULL);

  if (prerm == NULL)
    return FALSE;

  return g_subprocess_wait_check (prerm, cancellable, error);
}

static void
user_manager_vanished_cb (GDBusConnection *system_bus,
                          const gchar *name,
                          gpointer user_data)
{
  RcBundleManager *self = RC_BUNDLE_MANAGER (user_data);
  g_autoptr (GMutexLocker) lock =
      g_mutex_locker_new (&self->user_managers_lock);

  DEBUG ("Removing %s", name);
  g_hash_table_remove (self->user_managers, name);
}

/*
 * rc_bundle_manager_add_user_manager:
 * @self: the bundle manager
 * @system_bus_unique_name: a unique name on the system bus
 *
 * Add a user manager (in practice this means Canterbury) to be notified
 * when an app-bundle is about to be removed.
 */
void
rc_bundle_manager_add_user_manager (RcBundleManager *self,
                                    const gchar *system_bus_unique_name)
{
  g_autoptr (GMutexLocker) lock = NULL;

  g_return_if_fail (RC_IS_BUNDLE_MANAGER (self));
  g_return_if_fail (g_dbus_is_unique_name (system_bus_unique_name));

  DEBUG ("Adding %s", system_bus_unique_name);

  lock = g_mutex_locker_new (&self->user_managers_lock);

  if (!g_hash_table_contains (self->user_managers, system_bus_unique_name))
    {
      guint watch =
          g_bus_watch_name_on_connection (self->system_bus,
                                          system_bus_unique_name,
                                          G_BUS_NAME_WATCHER_FLAGS_NONE,
                                          NULL, user_manager_vanished_cb,
                                          self, NULL);

      g_hash_table_insert (self->user_managers,
                           g_strdup (system_bus_unique_name),
                           GUINT_TO_POINTER (watch));
    }
}

/*
 * rc_bundle_manager_postinst_sync:
 * @self: the bundle manager
 * @bundle_id: the bundle
 * @bundle: the open and locked bundle
 * @version: the current version
 * @error: used to raise a %RC_ERROR on failure
 *
 * Perform post-installation actions for @bundle. Like dpkg's postinst
 * maintainer script, this is called after installation, upgrade or rollback,
 * or when the prerm fails.
 */
static gboolean
rc_bundle_manager_postinst_sync (RcBundleManager *self,
                                 const gchar *bundle_id,
                                 RcBundle *bundle,
                                 RcBundleVersion *version,
                                 GCancellable *cancellable,
                                 GError **error)
{
  g_autoptr (GSubprocess) postinst = NULL;
  g_autofree gchar *program = g_find_program_in_path ("canterbury-postinst");

  if (program == NULL)
    {
      INFO ("Not running canterbury-postinst: not found");
      return TRUE;
    }

  DEBUG ("Running canterbury-postinst \"%s\" \"%s\"...", bundle_id,
         version->version);

  /* We run a subprocess (provided by Canterbury) so that we can transition
   * to a more restrictive AppArmor profile, rather than parsing app-supplied
   * data in the highly-privileged Ribchester process. */
  postinst = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                               error,
                               "canterbury-postinst",
                               bundle_id,
                               version->version,
                               NULL);

  if (postinst == NULL)
    return FALSE;

  return g_subprocess_wait_check (postinst, cancellable, error);
}

/*
 * rc_bundle_manager_open_bundle:
 * @self: the bundle manager
 * @bundle_id: the bundle ID
 * @flags: flags affecting how we open the bundle
 * @bundle_out: (out caller-allocates) (optional): where to write the path
 *  and file descriptor of the bundle
 *  (must be uninitialized or cleared, existing contents will be overwritten
 *  without freeing)
 * @error: used to raise a %RC_ERROR on failure
 *
 * Attempt to open a file descriptor representing @bundle_id.
 *
 * If @bundle_id is syntactically invalid, raise %RC_ERROR_INVALID_ARGUMENT.
 *
 * If @bundle_id is not already present and %OPEN_CREATE is not in @flags,
 * raise %RC_ERROR_NOT_FOUND.
 *
 * If %OPEN_CREATE is in @flags, create an empty directory for the bundle
 * if necessary.
 *
 * This function takes an exclusive filesystem lock to protect against
 * concurrent access to a bundle. This lock is held until @bundle_out is
 * cleared; if another thread or process has the lock, this function will
 * block until it can be acquired. If the #RcBundleManager deletes a bundle,
 * it must be holding the lock, and then may delete the lock file as its
 * last action before releasing the lock. This cannot conflict with another
 * thread, because no other thread could be holding the lock, and any other
 * thread that would operate on that bundle would create a new lock file
 * in the same place before proceeding.
 *
 * Returns: %TRUE if we could open or create the bundle
 */
static gboolean
rc_bundle_manager_open_bundle (RcBundleManager *self,
                               const gchar *bundle_id,
                               OpenFlags flags,
                               RcBundle *bundle_out,
                               GError **error)
{
  g_autofree gchar *built_in_path = NULL;
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;

  g_return_val_if_fail (bundle_id != NULL, FALSE);
  g_return_val_if_fail (flags == (flags & OPEN_CREATE), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (bundle_out != NULL)
    rc_bundle_init (bundle_out);

  if (!cby_is_bundle_id (bundle_id))
    {
      g_set_error (error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                   "Not a valid bundle ID: \"%s\"", bundle_id);
      return FALSE;
    }

  built_in_path = g_build_filename (CBY_PATH_PREFIX_BUILT_IN_BUNDLE,
                                    bundle_id, NULL);

  /* Not bothering to do fd-based I/O here because if /usr doesn't have
   * stable and trustworthy contents, we have far worse problems,
   * like the provenance of the libraries linked into this process. */
  if (g_file_test (built_in_path, G_FILE_TEST_EXISTS))
    bundle.type = CBY_PROCESS_TYPE_BUILT_IN_BUNDLE;
  else
    bundle.type = CBY_PROCESS_TYPE_STORE_BUNDLE;

  bundle.lock = openat (self->locks_fd, bundle_id,
                        O_WRONLY | O_CREAT | O_NOFOLLOW | O_CLOEXEC,
                        0600);

  if (bundle.lock < 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to open lock file \"%s/%s\": %s", self->locks_path,
                   bundle_id, g_strerror (errno));
      return FALSE;
    }

  /* flock(2) locks are per-file-descriptor, so this is independent of other
   * threads in this process. If another thread has this bundle open,
   * this will block until it is released. */
  while (flock (bundle.lock, LOCK_EX) < 0)
    {
      if (errno != EINTR)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Unable to lock \"%s/lock\": %s", bundle.path,
                       g_strerror (errno));
          return FALSE;
        }
    }

  if ((flags & OPEN_CREATE) &&
      mkdirat (self->bundles_fd, bundle_id, 0755) < 0 && errno != EEXIST)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s/%s\": %s", self->bundles_path,
                   bundle_id, g_strerror (errno));
      return FALSE;
    }

  bundle.variable_mount_point = g_build_filename (self->variable_mounts_path,
                                                  bundle_id, NULL);

  switch (bundle.type)
    {
    case CBY_PROCESS_TYPE_STORE_BUNDLE:
      bundle.store_mount_point = g_build_filename (self->store_mounts_path,
                                                   bundle_id, NULL);
      break;

    case CBY_PROCESS_TYPE_BUILT_IN_BUNDLE:
      /* its static content lives in /usr, no mount point is needed */
      break;

    case CBY_PROCESS_TYPE_UNKNOWN:
    case CBY_PROCESS_TYPE_PLATFORM:
    default:
      g_assert_not_reached ();
    }

  bundle.path = g_build_filename (self->bundles_path, bundle_id, NULL);
  bundle.fd = rc_open_dir_at (self->bundles_path, self->bundles_fd, bundle_id,
                              RC_PATH_FLAGS_NOFOLLOW_SYMLINKS, error);

  if (bundle.fd < 0)
    return FALSE;

  g_assert (rc_bundle_is_open (&bundle));

  if (bundle_out != NULL)
    rc_bundle_steal_from (bundle_out, &bundle);

  return TRUE;
}

/*
 * rc_bundle_open_version:
 * @self: a bundle populated via rc_bundle_manager_open()
 * @version_number: the version to open or create
 * @flags: flags affecting how we open the version's directory and subvolumes
 * @version_out: (out caller-allocates) (optional): where to write the paths
 *  and file descriptors of the subdirectory (must be uninitialized or cleared,
 *  existing contents will be overwritten without freeing)
 * @error: used to raise a %RC_ERROR on failure
 *
 * Attempt to open a directory representing the given version of @self.
 *
 * If @version is not already present and %OPEN_CREATE is not in @flags,
 * raise %RC_ERROR_NOT_FOUND.
 *
 * If %OPEN_CREATE is in @flags, create a directory with empty subvolumes
 * if necessary.
 *
 * Returns: %TRUE if we could open or create the directory
 */
static gboolean
rc_bundle_open_version (RcBundle *self,
                        const gchar *version_number,
                        OpenFlags flags,
                        RcBundleVersion *version_out,
                        GError **error)
{
  g_autofree gchar *dirname = NULL;
  g_auto (RcBundleVersion) version = RC_BUNDLE_VERSION_INIT;
  GError *local_error = NULL;
  const gchar *slash;

  g_return_val_if_fail (rc_bundle_is_open (self), FALSE);
  g_return_val_if_fail (version_number != NULL, FALSE);
  g_return_val_if_fail (flags == (flags & (OPEN_CREATE|OPEN_INCOMPLETE)),
                        FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (version_out != NULL)
    rc_bundle_version_init (version_out);

  if (!check_valid_version (version_number, error))
    return FALSE;

  dirname = g_strconcat (VERSION_PREFIX, version_number, NULL);

  if ((flags & OPEN_CREATE) &&
      mkdirat (self->fd, dirname, 0755) < 0 && errno != EEXIST)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s/%s\": %s",
                   self->path, dirname, g_strerror (errno));
      return FALSE;
    }

  version.path = g_build_filename (self->path, dirname, NULL);

  /* Small optimization: version.directory and version.version point into
   * version.path to avoid having to g_strdup() them separately */
  slash = strrchr (version.path, G_DIR_SEPARATOR);
  g_assert (slash != NULL);
  version.directory = slash + 1;
  g_assert (strcmp (dirname, version.directory) == 0);
  g_assert (g_str_has_prefix (version.directory, VERSION_PREFIX));
  version.version = version.directory + strlen (VERSION_PREFIX);
  g_assert (strcmp (version_number, version.version) == 0);

  version.fd = rc_open_dir_at (self->path, self->fd, dirname,
                               RC_PATH_FLAGS_NOFOLLOW_SYMLINKS, error);

  if (version.fd < 0)
    return FALSE;

  if ((flags & OPEN_CREATE) &&
      !rc_create_bind_source_at (version.path, version.fd, SUBVOL_VARIABLE,
                                     0, 0, &local_error))
    {
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_EXISTS))
        {
          g_clear_error (&local_error);
        }
      else
        {
          g_propagate_error (error, local_error);
          return FALSE;
        }
    }

  if ((flags & OPEN_CREATE) &&
      !rc_create_bind_source_at (version.path, version.fd, SUBVOL_STATIC,
                                     0, 0, &local_error))
    {
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_EXISTS))
        {
          g_clear_error (&local_error);
        }
      else
        {
          g_propagate_error (error, local_error);
          return FALSE;
        }
    }

  version.variable_subvol_path = g_build_filename (version.path,
                                                   SUBVOL_VARIABLE, NULL);
  version.variable_subvol_fd = rc_open_dir_at (version.path, version.fd,
                                               SUBVOL_VARIABLE,
                                               RC_PATH_FLAGS_NOFOLLOW_SYMLINKS,
                                               &local_error);

  if (version.variable_subvol_fd < 0)
    {
      if ((flags & OPEN_INCOMPLETE) &&
          g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        {
          g_clear_error (&local_error);
        }
      else
        {
          g_propagate_error (error, local_error);
          return FALSE;
        }
    }

  if (self->type == CBY_PROCESS_TYPE_STORE_BUNDLE)
    {
      version.store_subvol_path = g_build_filename (version.path, SUBVOL_STATIC,
                                                    NULL);
      version.store_subvol_fd = rc_open_dir_at (version.path, version.fd,
                                                SUBVOL_STATIC,
                                                RC_PATH_FLAGS_NOFOLLOW_SYMLINKS,
                                                error);

      if (version.store_subvol_fd < 0)
        return FALSE;
    }

  g_assert (rc_bundle_version_is_open (&version));

  if (version_out != NULL)
    rc_bundle_version_steal_from (version_out, &version);

  return TRUE;
}

static gboolean
rc_bundle_manager_mount_unlocked (RcBundleManager *self,
                                  const gchar *bundle_id,
                                  RcBundle *bundle,
                                  RcBundleVersion *version,
                                  GError **error)
{
  g_auto (RcFileDescriptor) mount_variable_on_fd = -1;
  GStatBuf current_stat;
  GStatBuf mount_point_stat;

  DEBUG ("mounting variable data at \"%s\"", bundle->variable_mount_point);

  if (mkdirat (self->variable_mounts_fd, bundle_id, 0755) != 0 &&
      errno != EEXIST)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s\": %s", bundle->variable_mount_point,
                   g_strerror (errno));
      return FALSE;
    }

  mount_variable_on_fd = rc_open_dir_at (self->variable_mounts_path,
                                         self->variable_mounts_fd, bundle_id,
                                         RC_PATH_FLAGS_NONE, error);

  if (mount_variable_on_fd < 0)
    return FALSE;

  if (fstat (version->variable_subvol_fd, &current_stat) < 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to read metadata of \"%s\": %s",
                   version->variable_subvol_path, g_strerror (errno));
      return FALSE;
    }

  if (fstat (mount_variable_on_fd, &mount_point_stat) < 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to read metadata of \"%s\": %s",
                   bundle->variable_mount_point, g_strerror (errno));
      return FALSE;
    }

  if (current_stat.st_dev == mount_point_stat.st_dev &&
      current_stat.st_ino == mount_point_stat.st_ino)
    {
      DEBUG ("\"%s\" appears to be mounted on \"%s\" already",
             version->variable_subvol_path, bundle->variable_mount_point);
    }
  else if (!rc_bind_mount (version->variable_subvol_path,
                           bundle->variable_mount_point, error))
    {
      return FALSE;
    }

  if (bundle->type == CBY_PROCESS_TYPE_STORE_BUNDLE)
    {
      g_auto (RcFileDescriptor) mount_store_on_fd = -1;

      DEBUG ("mounting static data at \"%s\"", bundle->store_mount_point);

      if (mkdirat (self->store_mounts_fd, bundle_id, 0755) != 0 &&
          errno != EEXIST)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Failed to create \"%s/%s\": %s",
                       self->store_mounts_path,
                       bundle_id, g_strerror (errno));
          return FALSE;
        }

      mount_store_on_fd = rc_open_dir_at (self->store_mounts_path,
                                          self->store_mounts_fd, bundle_id,
                                          RC_PATH_FLAGS_NONE, error);

      if (mount_store_on_fd < 0)
        return FALSE;

      if (fstat (version->store_subvol_fd, &current_stat) < 0)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Failed to read metadata of \"%s\": %s",
                       version->store_subvol_path, g_strerror (errno));
          return FALSE;
        }

      if (fstat (mount_store_on_fd, &mount_point_stat) < 0)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Failed to read metadata of \"%s\": %s",
                       bundle->store_mount_point, g_strerror (errno));
          return FALSE;
        }

      if (current_stat.st_dev == mount_point_stat.st_dev &&
          current_stat.st_ino == mount_point_stat.st_ino)
        {
          DEBUG ("\"%s\" appears to be mounted on \"%s\" already",
                 version->store_subvol_path, bundle->store_mount_point);
        }
      else if (!rc_bind_mount (version->store_subvol_path,
                               bundle->store_mount_point, error))
        {
          return FALSE;
        }
    }

  return TRUE;
}

/*
 * rc_bundle_read_link:
 * @self: an app-bundle
 * @role: a role symlink such as %ROLE_CURRENT
 * @directory_out: (out) (transfer full) (optional): the directory pointed to
 * @version_out: (out) (transfer full) (optional): the version represented by
 *  that directory
 * @error: used to raise a %RC_ERROR on failure; this is %RC_ERROR_NOT_FOUND
 *  if and only if the symbolic link @role does not exist
 *
 * Read the symbolic link @role. Note that this is not atomic, and is prone
 * to time-of-check/time-of-use bugs if not used carefully.
 *
 * Returns: the version pointed to by @role
 */
static gboolean
rc_bundle_read_link (RcBundle *self,
                     const gchar *role,
                     gchar **directory_out,
                     gchar **version_number_out,
                     GError **error)
{
  gssize symlink_length;
  char buf[MAX_VERSION_LENGTH + MAX_VERSION_OVERHEAD + 1];

  g_return_val_if_fail (rc_bundle_is_open (self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (directory_out != NULL)
    *directory_out = NULL;

  if (version_number_out != NULL)
    *version_number_out = NULL;

  symlink_length = readlinkat (self->fd, role, buf, sizeof (buf) - 1);

  if (symlink_length < 0)
    {
      int saved_errno = errno;
      RcError code;

      switch (saved_errno)
        {
        case ENOENT:
          code = RC_ERROR_NOT_FOUND;
          break;

        default:
          code = RC_ERROR_FAILED;
        }

      g_set_error (error, RC_ERROR, code,
                   "Unable to dereference symbolic link \"%s/%s\": %s",
                   self->path, role, g_strerror (saved_errno));
      return FALSE;
    }

  if ((gsize) symlink_length > sizeof (buf) - 1)
    {
      /* readlinkat returns how many bytes it wrote, so we cannot say
       * anything beyond a lower bound. */
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Symbolic link \"%s/%s\" target too long: it is "
                   "%" G_GSSIZE_FORMAT " bytes or more",
                   self->path, role, symlink_length);
      return FALSE;
    }

  buf[symlink_length] = '\0';

  if (!g_str_has_prefix (buf, VERSION_PREFIX))
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Symbolic link \"%s/%s\" has invalid target: \"%s\"",
                   self->path, role, buf);
      return FALSE;
    }

  if (!check_valid_version (buf + strlen (VERSION_PREFIX), error))
    {
      g_prefix_error (error,
                      "Symbolic link \"%s/%s\" points to invalid version: "
                      "\"%s\": ",
                      self->path, role, buf + 1);
      return FALSE;
    }

  if (directory_out != NULL)
    *directory_out = g_strdup (buf);

  if (version_number_out != NULL)
    *version_number_out = g_strdup (buf + strlen (VERSION_PREFIX));

  return TRUE;
}

/*
 * rc_bundle_open_role:
 * @self: an app-bundle
 * @role: a role symlink such as %ROLE_CURRENT
 * @version_out: (out caller-allocates) (optional):
 *  where to write the paths and file descriptors of the
 *  subdirectory (must be uninitialized or cleared, existing contents will
 *  be overwritten without freeing)
 * @error: used to raise a %RC_ERROR on failure; in particular,
 *  %RC_ERROR_NOT_FOUND if the @role symlink either does not exist, or exists
 *  but points to a nonexistent version
 *
 * Open a version within an app-bundle by its role, for example
 * %ROLE_CURRENT.
 *
 * Returns: %TRUE if the version with that role was successfully opened
 */
static gboolean
rc_bundle_open_role (RcBundle *self,
                     const gchar *role,
                     RcBundleVersion *version_out,
                     GError **error)
{
  g_auto (RcBundleVersion) version = RC_BUNDLE_VERSION_INIT;
  struct stat direct_stat_buf;
  struct stat via_symlink_stat_buf;
  g_autofree gchar *version_number = NULL;

  g_return_val_if_fail (rc_bundle_is_open (self), FALSE);
  g_return_val_if_fail (role != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (version_out != NULL)
    rc_bundle_version_init (version_out);

  if (!rc_bundle_read_link (self, role, NULL, &version_number, error))
    return FALSE;

  if (!rc_bundle_open_version (self, version_number, OPEN_NONE, &version,
                               error))
    return FALSE;

  /* Try to mitigate time-of-check/time-of-use issues by checking that the
   * symlink still points to that same version. */

  if (fstat (version.fd, &direct_stat_buf) < 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to read metadata of \"%s\": %s",
                   version.path, g_strerror (errno));
      return FALSE;
    }

  if (fstatat (self->fd, role, &via_symlink_stat_buf, 0))
    {
      int saved_errno = errno;
      RcError code;

      switch (saved_errno)
        {
        case ENOENT:
          code = RC_ERROR_NOT_FOUND;
          break;

        default:
          code = RC_ERROR_FAILED;
        }

      g_set_error (error, RC_ERROR, code,
                   "Unable to read metadata following symlink \"%s/%s\": %s",
                   self->path, role, g_strerror (saved_errno));
      return FALSE;
    }

  if (direct_stat_buf.st_dev != via_symlink_stat_buf.st_dev ||
      direct_stat_buf.st_ino != via_symlink_stat_buf.st_ino)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Target of symlink \"%s/%s\" changed during reading",
                   self->path, role);
      return FALSE;
    }

  if (version_out != NULL)
    rc_bundle_version_steal_from (version_out, &version);

  return TRUE;
}

/*
 * rc_bundle_delete_subdirectory:
 * @bundle: a bundle containing @version
 * @directory: the directory corresponding to a bundle version number, or
 *  the special token %SUBDIR_UNVERSIONED
 * @error: used to raise an %RC_ERROR on failure
 *
 * Delete the subvolumes and subdirectory @directory.
 *
 * %RC_ERROR_NOT_FOUND is raised if the subdirectory does not exist.
 * However, if the subdirectory exists but does not contain the
 * subvolumes we expect, that is not considered to be an error.
 */
static gboolean
rc_bundle_delete_subdirectory (RcBundle *self,
                               const gchar *directory,
                               GError **error)
{
  /* A version is a subdirectory containing subvolumes with predictable names
   * taken from a small set; the older "unversioned" subdirectory was a
   * subdirectory containing subvolumes with predictable names from a
   * different small set. For simplicity, we just take the union of those two
   * and delete all of them. */
  static const gchar * const known_names[] =
  {
    SUBVOL_STATIC,
    SUBVOL_VARIABLE,
    SUBVOL_COMBINED,
    ROLE_CURRENT,
    ROLE_INSTALLING,
    ROLE_ROLLBACK,
    ROLE_UNINSTALLED
  };
  g_autofree gchar *path = NULL;
  g_auto (RcFileDescriptor) fd = -1;
  GError *local_error = NULL;
  gsize i;

  g_return_val_if_fail (rc_bundle_is_open (self), FALSE);
  g_return_val_if_fail (directory != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  path = g_build_filename (self->path, directory, NULL);

  for (i = 0; i < G_N_ELEMENTS (known_names); i++)
    {
      if (!rc_delete_bind_source (path, known_names[i], &local_error))
        {
          if (g_error_matches (local_error, RC_ERROR,
                               RC_ERROR_NOT_FOUND))
            {
              g_clear_error (&local_error);
            }
          else
            {
              g_propagate_error (error, local_error);
              return FALSE;
            }
        }
    }

  rc_file_descriptor_clear (&fd);

  /* Now the directory should be empty, so we can do the equivalent
   * of rmdir. */
  if (unlinkat (self->fd, directory, AT_REMOVEDIR) == 0)
    {
      DEBUG ("removed directory \"%s/%s\"", self->path, directory);
    }
  else if (errno == ENOENT)
    {
      /* Weird, but in line with what we wanted, so don't complain. */
      DEBUG ("directory \"%s/%s\" has already vanished", self->path,
             directory);
    }
  else
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Cannot remove directory \"%s\": %s", path,
                   g_strerror (errno));
      return FALSE;
    }

  return TRUE;
}

typedef enum
{
  CLEAN_UP_FLAGS_NONE = 0,
  CLEAN_UP_FLAGS_DESTROY = (1 << 0),
} CleanUpFlags;

/*
 * rc_bundle_manager_clean_up_unlocked:
 *
 * Clean up unwanted files in @bundle:
 *
 * - `ROLE_` symbolic links that are only meant to exist during an operation
 * - version numbers in @bundle that do not correspond to one of the
 *   `ROLE_` symbolic links
 *
 * If %CLEAN_UP_FLAGS_DESTROY is used, additionally delete the
 * long-term-persistent symbolic links and the bundle directory.
 *
 * @bundle's lock must be held.
 */
static gboolean
rc_bundle_manager_clean_up_unlocked (RcBundleManager *self,
                                     const gchar *bundle_id,
                                     RcBundle *bundle,
                                     CleanUpFlags flags,
                                     GError **error)
{
  static const gchar * const persistent_links[] =
  {
    ROLE_CURRENT,
    ROLE_ROLLBACK,
    ROLE_UNINSTALLED
  };
  static const gchar * const transient_links[] =
  {
    ROLE_OLD_CURRENT,
    ROLE_OLD_ROLLBACK,
    ROLE_INSTALLING
  };
  g_autoptr (RcDirectoryIter) dir = NULL;
  RcBundleVersion persistent_versions[G_N_ELEMENTS (persistent_links)];
  GError *local_error = NULL;
  struct dirent *entry;
  gboolean ret = FALSE;
  gsize i;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (bundle_id != NULL, FALSE);
  g_return_val_if_fail (rc_bundle_is_open (bundle), FALSE);
  g_return_val_if_fail (flags == (flags & CLEAN_UP_FLAGS_DESTROY), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  /* This is separated from the similar loop immediately below so that we do
   * all the initialization before anything else. */
  for (i = 0; i < G_N_ELEMENTS (persistent_links); i++)
    rc_bundle_version_init (&persistent_versions[i]);

  for (i = 0; i < G_N_ELEMENTS (persistent_links); i++)
    {
      g_autofree gchar *tmp = g_strconcat (persistent_links[i], ".tmp", NULL);

      if (unlinkat (bundle->fd, tmp, 0) != 0 && errno != ENOENT)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Could not remove \"%s/%s\": %s", bundle->path, tmp,
                       g_strerror (errno));
          goto out;
        }

      if ((flags & CLEAN_UP_FLAGS_DESTROY) &&
          unlinkat (bundle->fd, persistent_links[i], 0) != 0 &&
          errno != ENOENT)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Could not remove \"%s/%s\": %s", bundle->path,
                       persistent_links[i], g_strerror (errno));
          goto out;
        }
    }

  for (i = 0; i < G_N_ELEMENTS (transient_links); i++)
    {
      g_autofree gchar *tmp = g_strconcat (transient_links[i], ".tmp", NULL);

      if (unlinkat (bundle->fd, transient_links[i], 0) != 0 && errno != ENOENT)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Could not remove \"%s/%s\": %s", bundle->path,
                       transient_links[i], g_strerror (errno));
          goto out;
        }

      if (unlinkat (bundle->fd, tmp, 0) != 0 && errno != ENOENT)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Could not remove \"%s/%s\": %s", bundle->path, tmp,
                       g_strerror (errno));
          goto out;
        }
    }

  for (i = 0; i < G_N_ELEMENTS (persistent_links); i++)
    {
      if (!rc_bundle_open_role (bundle, persistent_links[i],
                                &persistent_versions[i], &local_error))
        {
          if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
            {
              g_clear_error (&local_error);
            }
          else
            {
              g_propagate_error (error, local_error);
              goto out;
            }
        }
    }

  dir = rc_directory_iter_new (bundle->fd, error);

  if (dir == NULL)
    goto out;

  for (entry = readdir (dir); entry != NULL; entry = readdir (dir))
    {
      gboolean in_use = FALSE;

      if (!g_str_has_prefix (entry->d_name, VERSION_PREFIX))
        continue;

      for (i = 0; i < G_N_ELEMENTS (persistent_links); i++)
        {
          if (rc_bundle_version_is_open (&persistent_versions[i]) &&
              strcmp (persistent_versions[i].directory, entry->d_name) == 0)
            {
              in_use = TRUE;
              break;
            }
        }

      if (!in_use &&
          !rc_bundle_delete_subdirectory (bundle, entry->d_name, &local_error))
        {
          if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
            {
              /* Unlikely and would indicate concurrent modification -
               * dangerous! */
              g_prefix_error (&local_error, "Concurrent modification? ");
              local_error->code = RC_ERROR_FAILED;
            }

          g_propagate_error (error, local_error);
          goto out;
        }
    }

  if (unlinkat (self->bundles_fd, bundle_id, AT_REMOVEDIR) == 0 ||
      errno == ENOENT)
    {
      /* The bundle was successfully removed or didn't exist. Remove the
       * lockfile, too. */
      if (G_UNLIKELY (unlinkat (self->locks_fd, bundle_id, 0) != 0 &&
                      errno != ENOENT))
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Could not remove \"%s/%s\": %s", self->locks_path,
                       bundle_id, g_strerror (errno));
          goto out;
        }
    }
  else if (flags & CLEAN_UP_FLAGS_DESTROY)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Could not remove \"%s\": %s", bundle->path,
                   g_strerror (errno));
      goto out;
    }

  ret = TRUE;
out:
  for (i = 0; i < G_N_ELEMENTS (persistent_versions); i++)
    rc_bundle_version_clear (&persistent_versions[i]);

  return ret;
}

static gboolean rc_bundle_manager_mount_app_sync (RcBundleManager *self,
                                                  const gchar *bundle_id,
                                                  RcBundle *bundle_out,
                                                  RcBundleVersion *version_out,
                                                  GError **error);

/*
 * rc_bundle_manager_prepare_sync:
 * @self: the bundle manager
 * @bundle_id: a bundle
 * @user_id: a user, which must not be uid 0 (root)
 *  or %CBY_PROCESS_INFO_NO_USER_ID
 * @cancellable: (nullable): if not %NULL, may be used to cancel the operation
 * @error: used to raise a %RC_ERROR if the operation fails;
 *  in particular, if there is no such app-bundle, %RC_ERROR_NOT_FOUND
 *  is raised
 *
 * Prepare @bundle_id to be run by @user_id.
 *
 * This function is intended to be thread-safe. Do not interact with
 * global state in this function, except via the filesystem or while
 * holding a lock.
 *
 * Returns: the absolute path to the per-user variable data directory in the
 *  mounted app-bundle @bundle_id, or %NULL on error
 */
gchar *
rc_bundle_manager_prepare_sync (RcBundleManager *self,
                                const gchar *bundle_id,
                                guint user_id,
                                GCancellable *cancellable,
                                GError **error)
{
  g_autofree gchar *mounted_path = NULL;
  g_autofree gchar *users_path = NULL;
  g_autofree gchar *persistence_path = NULL;
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;
  g_auto (RcBundleVersion) current_version = RC_BUNDLE_VERSION_INIT;
  g_auto (RcFileDescriptor) mounted_fd = -1;
  g_auto (RcFileDescriptor) users_fd = -1;
  g_auto (RcFileDescriptor) persistence_path_fd = -1;
  GStatBuf stat_buf;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), NULL);
  g_return_val_if_fail (bundle_id != NULL, NULL);
  g_return_val_if_fail (user_id > 0, NULL);
  g_return_val_if_fail (user_id != CBY_PROCESS_INFO_NO_USER_ID, NULL);
  g_return_val_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable),
                        NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  if (!rc_bundle_manager_mount_app_sync (self, bundle_id, &bundle,
                                         &current_version, error))
    return FALSE;

  mounted_path = g_build_filename (self->variable_mounts_path, bundle_id, NULL);
  mounted_fd = rc_open_dir_at (self->variable_mounts_path,
                               self->variable_mounts_fd, bundle_id,
                               RC_PATH_FLAGS_NOFOLLOW_SYMLINKS, error);

  if (mounted_fd < 0)
    return FALSE;

  /* If the app-bundle is installed, it should already have a subvolume.
   * We don't actually verify here that it's a subvolume, just that
   * it's a directory. */
  if (fstat (mounted_fd, &stat_buf) != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to read metadata of \"%s\": %s", mounted_path,
                   g_strerror (errno));
      return FALSE;
    }

  /* Must have at least the bits rwx--x--x set, but not ----w--w- */
  if ((stat_buf.st_mode & (S_IWGRP|S_IWOTH)) != 0 ||
      (stat_buf.st_mode | (S_IRWXU|S_IXGRP|S_IXOTH)) != stat_buf.st_mode)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "App-bundle \"%s\" has unsuitable permissions (0%o, should "
                   "be 0711 or 0755)",
                   mounted_path, (guint) stat_buf.st_mode);
      return FALSE;
    }

  if (stat_buf.st_uid != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "App-bundle \"%s\" is owned by uid %u, should be root",
                   mounted_path, (guint) stat_buf.st_uid);
      return FALSE;
    }

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  /* ordinary users may traverse [/var]/Applications/$bundle_id/users, but not
   * read it */
  if (mkdirat (mounted_fd, "users", 0711) < 0 && errno != EEXIST)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s/users\": %s", mounted_path,
                   g_strerror (errno));
      return FALSE;
    }

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  /* most likely [/var]/Applications/$bundle_id/users */
  users_path = g_build_filename (mounted_path, "users", NULL);
  users_fd = rc_open_dir_at (mounted_path, mounted_fd, "users",
                             RC_PATH_FLAGS_NOFOLLOW_SYMLINKS, error);

  if (users_fd < 0)
    return FALSE;

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  /* It might have already existed, so check that it's safe */
  if (fstat (users_fd, &stat_buf) != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to read metadata of \"%s\": %s", users_path,
                   g_strerror (errno));
      return FALSE;
    }

  /* If it isn't owned by root, someone else must have created it;
   * not safe to go any further */
  if (stat_buf.st_uid != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "App-bundle users directory \"%s\" is owned by uid %u, "
                   "should be root",
                   users_path, (unsigned) stat_buf.st_uid);
      return FALSE;
    }

  /* Again, must have at least the bits rwx--x--x set, but not ----w--w- */
  if ((stat_buf.st_mode & (S_IWGRP|S_IWOTH)) != 0 ||
      (stat_buf.st_mode | (S_IRWXU|S_IXGRP|S_IXOTH)) != stat_buf.st_mode)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "App-bundle users directory \"%s\" has unsuitable "
                   "permissions (0%o, should be 0711 or 0755)",
                   users_path, (guint) (07777 & stat_buf.st_mode));
      return FALSE;
    }

  /* The $uid in [/var]/Applications/$bundle_id/users/$uid */
  persistence_path = g_strdup_printf ("%u", user_id);

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  /* This directory is private to its owning user, rwx------ */
  if (mkdirat (users_fd, persistence_path, 0700) < 0 && errno != EEXIST)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to create \"%s/%s\": %s", users_path,
                   persistence_path, g_strerror (errno));
      return FALSE;
    }

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  persistence_path_fd = rc_open_dir_at (users_path, users_fd, persistence_path,
                                        RC_PATH_FLAGS_NOFOLLOW_SYMLINKS,
                                        error);

  if (persistence_path_fd < 0)
    return FALSE;

  /* Again, it might have already existed, so check that it's OK */
  if (fstat (persistence_path_fd, &stat_buf) != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to read metadata of \"%s/%s\": %s",
                   users_path, persistence_path, g_strerror (errno));
      return FALSE;
    }

  /* If it already existed, it should be owned by user_id, unless we crashed
   * halfway through our last attempt to create it, in which case it should
   * be owned by root. If it did not already exist, it will be owned by
   * root. */
  if (stat_buf.st_uid != 0 && stat_buf.st_uid != user_id)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "App-bundle per-user directory \"%s/%s\" "
                   "is owned by uid %u, should be 0 or %u",
                   users_path, persistence_path,
                   (unsigned) stat_buf.st_uid, user_id);
      return FALSE;
    }

  /* If its permissions are not what we expect, chmod it 0700,
   * i.e. rwx------. (POSIX says this use of 07777 is portable.) */
  if ((stat_buf.st_mode & 07777) != 0700 &&
      fchmod (persistence_path_fd, 0700) < 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to chmod \"%s/%s\" to 0700: %s",
                   users_path, persistence_path, g_strerror (errno));
      return FALSE;
    }

  /* If root still owns it, give it away. Leave the group-owner as root. */
  if (stat_buf.st_uid == 0 && fchown (persistence_path_fd, user_id, 0) < 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Failed to change ownership of \"%s/%s\": %s",
                   users_path, persistence_path, g_strerror (errno));
      return FALSE;
    }

  return g_build_filename (users_path, persistence_path, NULL);
}

/*
 * rc_bundle_manager_remove_sync:
 * @self: the bundle manager
 * @bundle_id: a bundle
 * @error: used to raise a %RC_ERROR if the operation fails;
 *  if the bundle does not exist, that is counted as a success
 *
 * Remove @bundle_id completely.
 *
 * This function is intended to be thread-safe. Do not interact with
 * global state in this function, except via the filesystem or while
 * holding a lock.
 *
 * Returns: %TRUE on success
 */
gboolean
rc_bundle_manager_remove_sync (RcBundleManager *self,
                               const gchar *bundle_id,
                               GError **error)
{
  static const gchar * const known_links[] =
  {
    ROLE_CURRENT,
    ROLE_OLD_CURRENT,
    ROLE_INSTALLING,
    ROLE_ROLLBACK,
    ROLE_OLD_ROLLBACK,
    ROLE_UNINSTALLED
  };
  GError *local_error = NULL;
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;
  gsize i;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (bundle_id != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!rc_bundle_manager_open_bundle (self, bundle_id, OPEN_NONE,
                                      &bundle, &local_error))
    {
      if (!g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        {
          g_propagate_error (error, local_error);
          return FALSE;
        }

      /* doesn't exist, so it's very easy to remove! */
      g_clear_error (&local_error);
      return TRUE;
    }

  if (!rc_bundle_manager_prerm_sync (self, bundle_id, &bundle, NULL, error))
    {
      g_auto (RcBundleVersion) version = RC_BUNDLE_VERSION_INIT;

      /* try to set it back up again */
      if (!rc_bundle_open_role (&bundle, ROLE_CURRENT, &version, NULL))
        {
          DEBUG ("Bundle \"%s\" prerm failed, but unable to open \"%s\" "
                 "role, so not running postinst to recover", bundle_id,
                 ROLE_CURRENT);
        }
      else if (!rc_bundle_manager_postinst_sync (self, bundle_id,
                                                 &bundle, &version, NULL,
                                                 &local_error))
        {
          WARNING ("Unable to reconfigure \"%s\" version \"%s\" while "
                   "recovering from error: %s",
                   bundle_id, version.version, local_error->message);
          g_clear_error (&local_error);
        }

      return FALSE;
    }

  /* This function deliberately does not check whether the bundle is a
   * store or built-in app bundle. If it's a store app bundle, we will
   * delete its static and variable files. If it's a built-in app bundle,
   * its static files are elsewhere (in /usr) but we will still delete its
   * variable files, effectively providing a data-reset. */

  if (bundle.store_mount_point != NULL &&
      !rc_unmount (bundle.store_mount_point, &local_error))
    {
      /* We can't use logic like
       *
       * if (mounted)
       *   unmount
       *
       * because ribchester unmounts with MNT_DETACH, so unmounted volumes
       * stay in the mount table for a short time (until pending I/O
       * occurs and they are *actually* unmounted). */
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        {
          DEBUG ("\"%s\" not mounted, ignoring", bundle_id);
          g_clear_error (&local_error);
        }
      else
        {
          g_propagate_error (error, local_error);
          return FALSE;
        }
    }

  if (!rc_unmount (bundle.variable_mount_point, &local_error))
    {
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        {
          DEBUG ("\"%s\" not mounted, ignoring", bundle_id);
          g_clear_error (&local_error);
        }
      else
        {
          g_propagate_error (error, local_error);
          return FALSE;
        }
    }

  if (!rc_bundle_delete_subdirectory (&bundle, SUBDIR_UNVERSIONED,
                                      &local_error))
    {
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        {
          g_clear_error (&local_error);
        }
      else
        {
          g_propagate_error (error, local_error);
          return FALSE;
        }
    }

  for (i = 0; i < G_N_ELEMENTS (known_links); i++)
    {
      if (unlinkat (bundle.fd, known_links[i], 0) != 0 && errno != ENOENT)
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Could not remove \"%s/%s\": %s",
                       bundle.path, known_links[i], g_strerror (errno));
          return FALSE;
        }
    }

  if (!rc_bundle_manager_clean_up_unlocked (self, bundle_id, &bundle,
                                            CLEAN_UP_FLAGS_DESTROY, error))
    return FALSE;

  if (unlinkat (self->store_mounts_fd, bundle_id, AT_REMOVEDIR) != 0 &&
      errno != ENOENT)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Could not remove \"%s/%s\": %s", self->store_mounts_path,
                   bundle_id, g_strerror (errno));
      return FALSE;
    }

  if (unlinkat (self->variable_mounts_fd, bundle_id, AT_REMOVEDIR) != 0 &&
      errno != ENOENT)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Could not remove \"%s/%s\": %s", self->variable_mounts_path,
                   bundle_id, g_strerror (errno));
      return FALSE;
    }

  if (!rc_bundle_manager_update_component_index_sync (self, NULL, &local_error))
    {
      /* Tolerate failure to update the component index: we have already
       * had our side-effect */
      WARNING ("%s", local_error->message);
      g_clear_error (&local_error);
    }

  return TRUE;
}

/*
 * rc_bundle_manager_delete_rollback_snapshot_sync:
 * @self: the bundle manager
 * @bundle_id: a bundle
 * @error: used to raise a %RC_ERROR if the operation fails;
 *  in particular, if there is no such app-bundle or if it does not have
 *  a rollback snapshot, %RC_ERROR_NOT_FOUND is raised
 *
 * Remove the snapshot that enables @bundle_id to be rolled back, if any.
 *
 * This function is intended to be thread-safe. Do not interact with
 * global state in this function, except via the filesystem or while
 * holding a lock.
 *
 * Returns: %TRUE on success
 */
gboolean
rc_bundle_manager_delete_rollback_snapshot_sync (RcBundleManager *self,
                                                 const gchar *bundle_id,
                                                 GError **error)
{
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;
  g_auto (RcBundleVersion) unversioned = RC_BUNDLE_VERSION_INIT;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (bundle_id != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!rc_bundle_manager_open_bundle (self, bundle_id, OPEN_NONE, &bundle,
                                      error))
    return FALSE;

  if (bundle.type != CBY_PROCESS_TYPE_STORE_BUNDLE)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                   "\"%s\" is a built-in app-bundle and cannot have its "
                   "rollback subvolumes removed",
                   bundle_id);
      return FALSE;
    }

  if (unlinkat (bundle.fd, ROLE_ROLLBACK, 0) != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Could not remove \"%s/%s\": %s", bundle.path,
                   ROLE_ROLLBACK, g_strerror (errno));
      return FALSE;
    }

  if (!rc_bundle_manager_clean_up_unlocked (self, bundle_id, &bundle,
                                            CLEAN_UP_FLAGS_NONE, error))
    return FALSE;

  return TRUE;
}

/*
 * rc_bundle_manager_begin_install_sync:
 * @self: the bundle manager
 * @bundle_id: a bundle
 * @version: the new version number of the bundle
 * @error: used to raise a %RC_ERROR if the operation fails
 *
 * Begin the installation or upgrade of an app-bundle.
 *
 * This function is intended to be thread-safe. Do not interact with
 * global state in this function, except via the filesystem or while
 * holding a lock.
 *
 * Returns: the absolute path of the new subvolume into which the
 *  caller is expected to unpack the new static content before calling
 *  rc_bundle_manager_commit_install_sync()
 */
gchar *
rc_bundle_manager_begin_install_sync (RcBundleManager *self,
                                      const gchar *bundle_id,
                                      const gchar *version,
                                      GError **error)
{
  GError *local_error = NULL;
  GError *unwind_error = NULL;
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;
  g_auto (RcBundleVersion) old_version = RC_BUNDLE_VERSION_INIT;
  g_auto (RcFileDescriptor) new_version_fd = -1;
  g_autofree gchar *new_version_dir = NULL;
  g_autofree gchar *new_version_path = NULL;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), NULL);
  g_return_val_if_fail (bundle_id != NULL, NULL);
  g_return_val_if_fail (version != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  /* We check the version's syntax before creating the new subdirectory,
   * because we want to make the "installing" symlink first. */
  if (!check_valid_version (version, &local_error) ||
      !rc_bundle_manager_open_bundle (self, bundle_id, OPEN_CREATE, &bundle,
                                      &local_error))
    goto error;

  /* FIXME: maybe in future we want to support upgrading a built-in bundle
   * by masking it with a store bundle, like Android does? If we do,
   * RcBundleManager's assumptions will have to be checked; also,
   * we must be very careful about its provenance to avoid letting a
   * more-trusted app's data leak to a less-trusted app, similar to
   * <https://www.us-cert.gov/ncas/alerts/TA14-317A>. For now, disallow it. */
  if (bundle.type != CBY_PROCESS_TYPE_STORE_BUNDLE)
    {
      g_set_error (&local_error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                   "\"%s\" is a built-in app-bundle and cannot be upgraded",
                   bundle_id);
      goto error;
    }

  new_version_dir = g_strconcat (VERSION_PREFIX, version, NULL);

  /* Create the "installing" symlink before creating the new version
   * subdirectory. If we did it the other way round, then when we rolled back
   * a failed installation that had not yet created its "installing" symlink,
   * we wouldn't delete the partially-installed version.
   *
   * We fail on RC_ERROR_EXISTS here: we don't want to allow another
   * BeginInstall() call for the same app if the previous one was neither
   * committed nor rolled back.
   *
   * FIXME: cancel any pending installations during system boot so that
   * we can't get stuck */
  if (!rc_durable_symlink_at (new_version_dir, bundle.path, bundle.fd,
                              ROLE_INSTALLING, RC_PATH_FLAGS_NONE,
                              &local_error))
    goto error;

  /* We don't let rc_bundle_open_version() create it because we want
   * exclusivity, and it's considerably simpler to special-case that here
   * than to add an OPEN_EXCLUSIVE_CREATE. */
  if (mkdirat (bundle.fd, new_version_dir, 0755) != 0)
    {
      int saved_errno = errno;
      RcError code = RC_ERROR_FAILED;

      if (saved_errno == EEXIST)
        code = RC_ERROR_EXISTS;

      g_set_error (&local_error, RC_ERROR, code,
                   "Failed to create \"%s/%s\": %s",
                   bundle.path, new_version_dir, g_strerror (saved_errno));
      goto error_remove_installing_symlink;
    }

  new_version_path = g_build_filename (bundle.path, new_version_dir, NULL);
  new_version_fd = rc_open_dir_at (bundle.path, bundle.fd, new_version_dir,
                                   RC_PATH_FLAGS_NOFOLLOW_SYMLINKS,
                                   &local_error);

  /* We only create the static subvolume at this point, because that's
   * all we need to begin installation. We'll take a snapshot of the
   * variable data subvolume later. */
  if (new_version_fd < 0 ||
      !rc_create_bind_source_at (new_version_path, new_version_fd,
                                 SUBVOL_STATIC, 0, 0, &local_error))
    goto error_remove_new_version;

  /* If the ROLE_CURRENT subdirectory exists, unmount it but leave
   * it in place. Unlike UpgradeApp, we don't overwrite it until it's time
   * to commit; we also don't copy it from current to rollback until it's
   * time to commit, which means we preserve our ability to roll back
   * from the current version to an earlier one even if we cancel/abort the
   * installation before we finish. */

  DEBUG ("unmounting \"%s\"", bundle_id);

  /* OK, now we're reasonably confident we're going to proceed. Time to
   * start breaking the currently-installed version! */
  if (!rc_bundle_manager_prerm_sync (self, bundle_id, &bundle, NULL,
                                     &local_error))
    goto error_undo_prerm;

  /* This assumes it's a store app-bundle, but we already checked that. */
  if (!rc_unmount (bundle.store_mount_point, &local_error))
    {
      /* If the subvolume was already not mounted (in particular,
       * if it didn't exist at all), that's fine.
       * Anything else results in us aborting installation. */
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        g_clear_error (&local_error);
      else
        goto error_undo_prerm;
    }

  if (!rc_unmount (bundle.variable_mount_point, &local_error))
    {
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        g_clear_error (&local_error);
      else
        goto error_undo_prerm;
    }

  return g_build_filename (new_version_path, SUBVOL_STATIC, NULL);

  /* Error unwinding: do the opposite of what we did above, in reverse */
error_undo_prerm:
  if (!rc_bundle_open_role (&bundle, ROLE_CURRENT, &old_version, NULL))
    {
      DEBUG ("Unable to open previous version of \"%s\", so not recovering "
             "from failed prerm by re-running postinst", bundle_id);
    }
  else if (!rc_bundle_manager_postinst_sync (self, bundle_id, &bundle,
                                             &old_version, NULL,
                                             &unwind_error))
    {
      WARNING ("Unable to reconfigure \"%s\" version \"%s\" while recovering "
               "from error: %s",
               bundle_id, old_version.version, unwind_error->message);
      g_clear_error (&unwind_error);
    }

error_remove_new_version:
  if (!rc_bundle_delete_subdirectory (&bundle, new_version_dir, &unwind_error))
    {
      WARNING ("Unable to delete \"%s/%s\" while recovering from error: %s",
               bundle.path, new_version_dir, unwind_error->message);
      g_clear_error (&unwind_error);
    }

error_remove_installing_symlink:
  if (unlinkat (bundle.fd, ROLE_INSTALLING, 0) < 0 && errno != ENOENT)
    {
      WARNING ("Unable to delete \"%s/%s\" while recovering from error: %s",
               bundle.path, ROLE_INSTALLING, g_strerror (errno));
    }

error:
  /* If we're recovering from failure to start the initial installation, then
   * the bundle directory will probably be empty, so do the equivalent
   * of rmdir(). However, if we're recovering from failure to start an upgrade,
   * then it will still have `current`, `rollback` and/or `uninstalled`
   * symlinks and the corresponding versioned subdirectories, which we don't
   * want to delete. To achieve this without time-of-check/time-of-use issues,
   * we do the equivalent of rmdir() instead of removing it recursively, and
   * ignore the "directory not empty" error if we get one.
   */
  if (unlinkat (self->bundles_fd, bundle_id, AT_REMOVEDIR) == 0 ||
      errno == ENOENT)
    {
      /* The bundle was successfully removed or didn't exist. Remove the
       * lockfile, too. This really shouldn't fail (even for ENOENT),
       * because if we get here, we successfully opened and locked
       * the lock file already, which means no other thread can be holding
       * it; if some other thread or process has deleted the lock without
       * first locking it, then that would be a sign that the other thread
       * is not complying with our locking scheme. */
      if (G_UNLIKELY (unlinkat (self->locks_fd, bundle_id, 0) < 0))
        {
          WARNING ("Unable to remove \"%s/%s\" while recovering from error: %s",
                   self->locks_path, bundle_id, g_strerror (errno));
          return FALSE;
        }
    }
  else if (errno != ENOTEMPTY)
    {
      WARNING ("Unable to delete \"%s\" while recovering from error: %s",
               bundle.path, g_strerror (errno));
    }

  g_propagate_error (error, local_error);
  return NULL;
}

static void
begin_install_in_thread (GTask *task,
                         gpointer source_object,
                         gpointer task_data,
                         GCancellable *cancellable)
{
  GError *error = NULL;
  InstallData *id = task_data;
  gchar *result;

  result = rc_bundle_manager_begin_install_sync (source_object,
                                                 id->bundle_id,
                                                 id->version,
                                                 &error);
  if (result != NULL)
    g_task_return_pointer (task, result, g_free);
  else
    g_task_return_error (task, error);
}


gchar *
rc_bundle_manager_begin_install_finish (RcBundleManager *self,
                                         GAsyncResult *res,
                                         GError **error)
{
  return g_task_propagate_pointer (G_TASK (res), error);
}

void
rc_bundle_manager_begin_install_async (RcBundleManager *self,
                                       const gchar *bundle_id,
                                       const gchar *version,
                                       GCancellable *cancellable,
                                       GAsyncReadyCallback callback,
                                       gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  InstallData *id = install_data_new (bundle_id, version);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_task_data (task, id, install_data_free);
  g_task_run_in_thread (task, begin_install_in_thread);
}


/*
 * rc_bundle_manager_commit_install_sync:
 * @self: the bundle manager
 * @bundle_id: a bundle
 * @version: the new version number of the bundle
 * @error: used to raise a %RC_ERROR if the operation fails;
 *  in particular, if there is no such app-bundle or if it does not have
 *  an uncommitted install pending, %RC_ERROR_NOT_FOUND is raised
 *
 * Finish the installation of an app-bundle.
 *
 * This function is intended to be thread-safe. Do not interact with
 * global state in this function, except via the filesystem or while
 * holding a lock.
 *
 * Returns: %TRUE on success
 */
gboolean
rc_bundle_manager_commit_install_sync (RcBundleManager *self,
                                       const gchar *bundle_id,
                                       const gchar *version,
                                       GError **error)
{
  GError *local_error = NULL;
  GError *unwind_error = NULL;
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;
  g_auto (RcBundleVersion) old_version = RC_BUNDLE_VERSION_INIT;
  g_auto (RcBundleVersion) new_version = RC_BUNDLE_VERSION_INIT;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (bundle_id != NULL, FALSE);
  g_return_val_if_fail (version != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  /* If we have done BeginInstall(), then we should have:
   * - ROLE_INSTALLING pointing to the new version
   * - ROLE_CURRENT pointing to the old version, if any
   * - ROLE_ROLLBACK pointing to an even older version, if any
   */

  if (!rc_bundle_manager_open_bundle (self, bundle_id, OPEN_NONE, &bundle,
                                      &local_error))
    goto error;

  if (bundle.type != CBY_PROCESS_TYPE_STORE_BUNDLE)
    {
      g_set_error (&local_error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                   "\"%s\" is a built-in app-bundle and cannot be upgraded",
                   bundle_id);
      goto error;
    }

  if (!rc_bundle_open_version (&bundle, version, OPEN_INCOMPLETE,
                               &new_version, &local_error))
    goto error;

  /* We want to make sure that ROLE_CURRENT is either nonexistent, or
   * something to which we will be able to roll back if this all goes wrong. */
  if (!rc_bundle_open_role (&bundle, ROLE_CURRENT, &old_version,
                            &local_error))
    {
      if (!g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        goto error;

      g_clear_error (&local_error);
    }

  /* We should already have done this in BeginInstall(), but it's what
   * rc_delete_btrfs_subvolume() has traditionally done.
   * Not checking whether store_mount_point is NULL because we already
   * checked that it's a store app-bundle. */
  rc_unmount (bundle.store_mount_point, NULL);
  rc_unmount (bundle.variable_mount_point, NULL);

  if (rc_bundle_version_is_open (&old_version))
    {
      DEBUG ("copying old data from \"%s\"", old_version.variable_subvol_path);

      if (!rc_snapshot_bind_source (old_version.variable_subvol_path,
                                    new_version.path,
                                    SUBVOL_VARIABLE, &local_error))
        goto error;
    }
  else
    {
      if (!rc_create_bind_source_at (new_version.path, new_version.fd,
                                     SUBVOL_VARIABLE, 0, 0, &local_error))
        goto error;
    }

  new_version.variable_subvol_fd = rc_open_dir_at (new_version.path,
                                                   new_version.fd,
                                                   SUBVOL_VARIABLE,
                                                   RC_PATH_FLAGS_NOFOLLOW_SYMLINKS,
                                                   &local_error);

  if (new_version.variable_subvol_fd < 0)
    goto error_delete_variable;

  /* The current version is about to become the rollback version,
   * and the rollback version is going to become the obsolete version.
   * Make sure there is no symlink pointing to the obsolete version.
   * (We'll clean up unreferenced version subdirectories later.) */
  if (unlinkat (bundle.fd, ROLE_OLD_ROLLBACK, 0) < 0 && errno != ENOENT)
    {
      g_set_error (&local_error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to remove \"%s/%s\": %s", bundle.path,
                   ROLE_OLD_ROLLBACK, g_strerror (errno));
      goto error_delete_variable;
    }

  DEBUG ("renaming \"%s/%s\" to \"%s/%s\"", bundle.path,
         ROLE_ROLLBACK, bundle.path, ROLE_OLD_ROLLBACK);

  /* We'll delete this soon, but for now we hang onto it so we can
   * (try to) put it back on failure (error_put_back_rollback). */
  if (renameat (bundle.fd, ROLE_ROLLBACK, bundle.fd,
                ROLE_OLD_ROLLBACK) != 0 && errno != ENOENT)
    {
      g_set_error (&local_error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to rename \"%s/%s\" to \"%s/%s\": %s",
                   bundle.path, ROLE_ROLLBACK, bundle.path, ROLE_OLD_ROLLBACK,
                   g_strerror (errno));
      goto error_delete_variable;
    }

  DEBUG ("renaming \"%s/%s\" to \"%s/%s\"", bundle.path, ROLE_CURRENT,
         bundle.path, ROLE_ROLLBACK);

  /* We ignore ENOENT here because we might be doing a new installation. */
  if (renameat (bundle.fd, ROLE_CURRENT, bundle.fd, ROLE_ROLLBACK) != 0 &&
      errno != ENOENT)
    {
      g_set_error (&local_error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to rename \"%s/%s\" to \"%s/%s\": %s",
                   bundle.path, ROLE_CURRENT, bundle.path, ROLE_ROLLBACK,
                   g_strerror (errno));
      goto error_put_back_rollback;
    }

  /* Don't ignore ENOENT for this one: we can't commit anything if we don't
   * have the ROLE_INSTALLING subdirectory. This should never happen, because
   * we already successfully opened ROLE_INSTALLING - it would indicate
   * concurrent modification. */
  if (renameat (bundle.fd, ROLE_INSTALLING, bundle.fd, ROLE_CURRENT) != 0)
    {
      int saved_errno = errno;
      RcError code = RC_ERROR_FAILED;

      if (saved_errno == EEXIST)
        code = RC_ERROR_EXISTS;

      g_set_error (&local_error, RC_ERROR, code,
                   "Unable to rename \"%s/%s\" to \"%s/%s\": %s",
                   bundle.path, ROLE_INSTALLING, bundle.path, ROLE_CURRENT,
                   g_strerror (saved_errno));
      goto error_put_back_current;
    }

  /* From this point onwards, everything in the storage directory has reached
   * a stable state, so we don't do the error unwinding even if mounting
   * fails - reshuffling files in the storage directory is not going to help
   * if something failed in mounting, and maybe the mount operation will
   * work better after a reboot or something?
   *
   * As such, this is a good time to clean up the old rollback volume,
   * which was only needed for the error-unwinding code path. */
  if (unlinkat (bundle.fd, ROLE_OLD_ROLLBACK, 0) < 0 && errno != ENOENT)
    {
      g_set_error (&local_error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to remove \"%s/%s\": %s", bundle.path,
                   ROLE_OLD_ROLLBACK, g_strerror (errno));
      goto error;
    }

  if (!rc_bundle_manager_clean_up_unlocked (self, bundle_id, &bundle,
                                            CLEAN_UP_FLAGS_NONE, &local_error))
    goto error;

  if (!rc_bundle_manager_mount_unlocked (self, bundle_id, &bundle, &new_version,
                                         &local_error))
    goto error;

  if (!rc_bundle_manager_postinst_sync (self, bundle_id, &bundle, &new_version,
                                        NULL, &local_error))
    goto error;

  if (!rc_bundle_manager_update_component_index_sync (self, NULL, &local_error))
    goto error;

  return TRUE;

  /* Error unwinding: do the opposite of what we did above, in reverse */
error_put_back_current:
  if (renameat (bundle.fd, ROLE_ROLLBACK, bundle.fd, ROLE_CURRENT) != 0 &&
      errno != ENOENT)
    {
      WARNING ("Unable to undo move of \"%s/%s\" to \"%s/%s\" while "
               "recovering from error: %s",
               bundle.path, ROLE_CURRENT, bundle.path, ROLE_ROLLBACK,
               g_strerror (errno));
    }

error_put_back_rollback:
  if (renameat (bundle.fd, ROLE_OLD_ROLLBACK, bundle.fd, ROLE_ROLLBACK) != 0 &&
      errno != ENOENT)
    {
      WARNING ("Unable to undo move of \"%s/%s\" to \"%s/%s\" while "
               "recovering from error: %s",
               bundle.path, ROLE_ROLLBACK, bundle.path, ROLE_OLD_ROLLBACK,
               g_strerror (errno));
    }

error_delete_variable:
  if (!rc_delete_bind_source (new_version.path, SUBVOL_VARIABLE,
                              &unwind_error))
    {
      WARNING ("Unable to undo creation of \"%s/%s\" while recovering "
               "from error: %s", new_version.path, SUBVOL_VARIABLE,
               unwind_error->message);
      g_clear_error (&unwind_error);
    }

error:
  g_propagate_error (error, local_error);
  return FALSE;
}

static void
commit_install_in_thread (GTask *task,
                          gpointer source_object,
                          gpointer task_data,
                          GCancellable *cancellable)
{
  GError *error = NULL;
  InstallData *id = task_data;

  if (rc_bundle_manager_commit_install_sync (source_object,
                                             id->bundle_id, id->version,
                                             &error))
    g_task_return_boolean (task, TRUE);
  else
    g_task_return_error (task, error);
}


gboolean
rc_bundle_manager_commit_install_finish (RcBundleManager *self,
                                         GAsyncResult *res,
                                         GError **error)
{
  return g_task_propagate_boolean (G_TASK (res), error);
}

void
rc_bundle_manager_commit_install_async (RcBundleManager *self,
                                        const gchar *bundle_id,
                                        const gchar *version,
                                        GCancellable *cancellable,
                                        GAsyncReadyCallback callback,
                                        gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  InstallData *id = install_data_new (bundle_id, version);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_task_data (task, id, install_data_free);
  g_task_run_in_thread (task, commit_install_in_thread);
}

/*
 * rc_bundle_manager_roll_back_sync:
 * @self: the bundle manager
 * @bundle_id: a bundle
 * @error: used to raise a %RC_ERROR if the operation fails;
 *  in particular, if there is no such app-bundle or if it does not have
 *  an uncommitted install that we can cancel or an old version to which
 *  we can roll back, %RC_ERROR_NOT_FOUND is raised
 *
 * Roll back @bundle_id to an earlier version.
 * If there is an uncommitted installation that was started with
 * rc_bundle_manager_begin_install_sync(), cancel it instead.
 *
 * This function is intended to be thread-safe. Do not interact with
 * global state in this function, except via the filesystem or while
 * holding a lock.
 *
 * Returns: the subvolume that was restored, for information only
 *  (it may have been deleted or renamed during processing)
 */
gchar *
rc_bundle_manager_roll_back_sync (RcBundleManager *self,
                                  const gchar *bundle_id,
                                  GError **error)
{
  GError *local_error = NULL;
  GError *unwind_error = NULL;
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;
  g_auto (RcBundleVersion) broken_version = RC_BUNDLE_VERSION_INIT;
  g_auto (RcBundleVersion) restored_version = RC_BUNDLE_VERSION_INIT;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), NULL);
  g_return_val_if_fail (bundle_id != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  if (!rc_bundle_manager_open_bundle (self, bundle_id, OPEN_NONE, &bundle,
                                      &local_error))
    goto error;

  /* FIXME: later, we should have a way to take a snapshot of all
   * store app-bundles during OS upgrade, then roll them all back later,
   * preferably in one operation. For now this API is assumed not to be it. */
  if (bundle.type != CBY_PROCESS_TYPE_STORE_BUNDLE)
    {
      g_set_error (&local_error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                   "\"%s\" is a built-in app-bundle and cannot be rolled back",
                   bundle_id);
      goto error;
    }

  if (unlinkat (bundle.fd, ROLE_INSTALLING, 0) == 0)
    {
      /* We were installing a new version. Rollback consists of
       * deleting the ROLE_INSTALLING version and re-mounting the
       * ROLE_CURRENT version. Fall through. */
      DEBUG ("deleted \"%s/%s\"", bundle.path, ROLE_INSTALLING);

      if (!rc_bundle_open_role (&bundle, ROLE_CURRENT, &restored_version,
                                &local_error))
        {
          if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
            g_clear_error (&local_error);
          else
            goto error;
        }
    }
  else if (errno != ENOENT)
    {
      /* There was an installation in progress, but we failed to cancel it. */
      g_set_error (&local_error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to delete \"%s/%s\": %s", bundle.path,
                   ROLE_INSTALLING, g_strerror (errno));
      goto error;
    }
  else
    {
      /* There was no installation in progress: roll back to an
       * earlier version instead */
      g_clear_error (&local_error);

      /* This remains open across the rename(). Unix filesystem semantics
       * are such that this is OK */
      if (!rc_bundle_open_role (&bundle, ROLE_ROLLBACK, &restored_version,
                                &local_error))
        {
          g_prefix_error (&local_error, "No snapshot to roll back: ");
          goto error;
        }

      if (!rc_bundle_manager_prerm_sync (self, bundle_id, &bundle, NULL,
                                         &local_error))
        goto error_undo_prerm;

      /* We already checked that this is  a store app-bundle, so we know it
       * has this mount point. */
      if (rc_unmount (bundle.store_mount_point,
                                      &local_error))
        {
          /* We may need to re-mount it during error recovery. */
          if (!rc_bundle_open_role (&bundle, ROLE_CURRENT, &broken_version,
                                    &local_error))
            {
              /* We carry on anyway, because rolling back is part of how
               * we cope with a broken app-bundle. */
              WARNING ("Unable to open current version, will not be able to "
                       "re-mount bundle if an error occurs: %s",
                       local_error->message);
              g_clear_error (&local_error);
            }
        }
      else
        {
          /* If it isn't mounted, that's OK. */
          if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
            g_clear_error (&local_error);
          else
            goto error_undo_prerm;
        }

      if (rc_unmount (bundle.variable_mount_point,
                                      &local_error))
        {
          /* We may need to re-mount it during error recovery. Don't
           * open it for a second time if we already did. */
          if (!rc_bundle_version_is_open (&broken_version) &&
              rc_bundle_open_role (&bundle, ROLE_CURRENT, &broken_version,
                                    &local_error))
            {
              /* We carry on anyway, because rolling back is part of how
               * we cope with a broken app-bundle. */
              WARNING ("Unable to open current version, will not be able to "
                       "re-mount bundle if an error occurs: %s",
                       local_error->message);
              g_clear_error (&local_error);
            }
        }
      else
        {
          /* If it isn't mounted, that's OK. */
          if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
            g_clear_error (&local_error);
          else
            goto error_remount;
        }

      /* Move the existing version of the app, if any, out of the way.
       * We do this so that we can put it back while recovering from errors. */
      if (renameat (bundle.fd, ROLE_CURRENT, bundle.fd,
                    ROLE_OLD_CURRENT) != 0 && errno != ENOENT)
        {
          g_set_error (&local_error, RC_ERROR, RC_ERROR_FAILED,
                       "Unable to rename \"%s/%s\" to \"%s/%s\": %s",
                       bundle.path, ROLE_CURRENT, bundle.path,
                       ROLE_OLD_CURRENT, g_strerror (errno));
          goto error_remount;
        }

      /* The rollback version becomes the new current version.
       * If there isn't one, that's an error (it shouldn't happen in
       * practice, because we already opened it successfully, but
       * there's a gap between time-of-check and time-of-use). */
      if (renameat (bundle.fd, ROLE_ROLLBACK, bundle.fd, ROLE_CURRENT) < 0)
        {
          int saved_errno = errno;
          RcError code = RC_ERROR_FAILED;

          if (saved_errno == EEXIST)
            code = RC_ERROR_EXISTS;

          /* shouldn't happen in practice: we already opened the rollback
           * snapshot successfully */
          if (saved_errno == ENOENT)
            code = RC_ERROR_NOT_FOUND;

          g_set_error (&local_error, RC_ERROR, code,
                       "Unable to roll back \"%s\": cannot rename \"%s/%s\" "
                       "to \"%s/%s\": %s",
                       bundle_id, bundle.path, ROLE_ROLLBACK, bundle.path,
                       ROLE_CURRENT, g_strerror (saved_errno));
          goto error_put_back_current;
        }
    }

  if (rc_bundle_version_is_open (&restored_version))
    {
      if (!rc_bundle_manager_mount_unlocked (self, bundle_id, &bundle,
                                             &restored_version, &local_error) ||
          !rc_bundle_manager_postinst_sync (self, bundle_id, &bundle,
                                            &restored_version, NULL,
                                            &local_error))
        goto error_put_back_current;
    }

  if (!rc_bundle_manager_clean_up_unlocked (self, bundle_id, &bundle,
                                            CLEAN_UP_FLAGS_NONE,
                                            &local_error) ||
      !rc_bundle_manager_update_component_index_sync (self, NULL, &local_error))
    {
      /* We've already had our side-effects, and with %ROLE_OLD_CURRENT
       * maybe having been deleted we probably can't undo them, so don't
       * raise an error about this: we don't want the caller to try again
       * and roll back further than they intended. Unreferenced versions
       * will eventually be cleaned up by noticing that they aren't
       * referenced, if possible. */
      WARNING ("%s", local_error->message);
      g_clear_error (&local_error);
    }

  if (!rc_bundle_version_is_open (&restored_version))
    {
      /* We have rolled back to "no version". */
      return g_strdup ("");
    }

  return g_strdup (restored_version.version);

  /* Error unwinding: do the opposite of what we did above, in reverse */
error_put_back_current:
  if (renameat (bundle.fd, ROLE_OLD_CURRENT, bundle.fd, ROLE_CURRENT) != 0 &&
      errno != ENOENT)
    {
      WARNING ("Unable to undo move of \"%s/%s\" to \"%s/%s\" while "
               "recovering from error: %s",
               bundle.path, ROLE_CURRENT, bundle.path, ROLE_OLD_CURRENT,
               g_strerror (errno));
    }

error_undo_prerm:
  if (rc_bundle_version_is_open (&broken_version) &&
      !rc_bundle_manager_postinst_sync (self, bundle_id, &bundle,
                                        &broken_version, NULL, &unwind_error))
    {
      WARNING ("Unable to reconfigure \"%s\" while recovering from error: %s",
               bundle_id, unwind_error->message);
      g_clear_error (&unwind_error);
    }

error_remount:
  if (rc_bundle_version_is_open (&broken_version) &&
      !rc_bundle_manager_mount_unlocked (self, bundle_id, &bundle,
                                         &broken_version, &unwind_error))
    {
      WARNING ("Unable to re-mount \"%s\" while recovering from error: %s",
               bundle_id, unwind_error->message);
      g_clear_error (&unwind_error);
    }

error:
  g_propagate_error (error, local_error);
  return NULL;
}

/*
 * rc_bundle_manager_uninstall_sync:
 * @self: the bundle manager
 * @bundle_id: a bundle
 * @error: used to raise a %RC_ERROR if the operation fails;
 *  in particular, if there is no such app-bundle or if it does not have
 *  a current/active version, %RC_ERROR_NOT_FOUND is raised
 *
 * Mark @bundle_id as uninstalled and make it unavailable, but do not
 * actually remove it. It can be restored with
 * rc_bundle_manager_reinstall_sync() or removed completely with
 * rc_bundle_manager_remove_sync().
 *
 * This function is intended to be thread-safe. Do not interact with
 * global state in this function, except via the filesystem or while
 * holding a lock.
 *
 * Returns: %TRUE on success
 */
gboolean
rc_bundle_manager_uninstall_sync (RcBundleManager *self,
                                  const gchar *bundle_id,
                                  GError **error)
{
  GError *local_error = NULL;
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;
  g_auto (RcBundleVersion) uninstalling_version = RC_BUNDLE_VERSION_INIT;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (bundle_id != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!rc_bundle_manager_open_bundle (self, bundle_id, OPEN_NONE, &bundle,
                                      &local_error) ||
      !rc_bundle_open_role (&bundle, ROLE_CURRENT, &uninstalling_version,
                            &local_error))
    goto error;

  /* FIXME: do we want a way to mark built-in app-bundles as disabled, too? */
  if (bundle.type != CBY_PROCESS_TYPE_STORE_BUNDLE)
    {
      g_set_error (&local_error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                   "\"%s\" is a built-in app-bundle and cannot be uninstalled",
                   bundle_id);
      goto error;
    }

  if (!rc_bundle_manager_prerm_sync (self, bundle_id, &bundle, NULL,
                                     &local_error))
    goto error;

  /* Unmount the app subvolume, if mounted. We already checked that this is
   * a store app-bundle, so we know this mount point is non-NULL. */
  if (!rc_unmount (bundle.store_mount_point, &local_error))
    {
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        g_clear_error (&local_error);
      else
        goto error;
    }

  if (!rc_unmount (bundle.variable_mount_point, &local_error))
    {
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND))
        g_clear_error (&local_error);
      else
        goto error;
    }

  /* Atomically rename the ROLE_CURRENT symbolic link to ROLE_UNINSTALLED,
   * overwriting ROLE_UNINSTALLED. */
  if (renameat (bundle.fd, ROLE_CURRENT, bundle.fd, ROLE_UNINSTALLED) < 0)
    {
      int saved_errno = errno;
      RcError code = RC_ERROR_FAILED;

      if (saved_errno == ENOENT)
        code = RC_ERROR_NOT_FOUND;

      g_set_error (&local_error, RC_ERROR, code,
                   "Unable to rename \"%s/" ROLE_CURRENT "\" to \""
                   ROLE_UNINSTALLED "\": %s",
                   bundle.path, g_strerror (errno));
      goto error;
    }

  if (!rc_bundle_manager_clean_up_unlocked (self, bundle_id, &bundle,
                                            CLEAN_UP_FLAGS_NONE,
                                            &local_error) ||
      !rc_bundle_manager_update_component_index_sync (self, NULL, &local_error))
    {
      /* We've already had our side-effects, so don't raise an error
       * about this. Unreferenced versions will eventually be cleaned up by
       * noticing that they aren't referenced, if possible. */
      WARNING ("%s", local_error->message);
      g_clear_error (&local_error);
    }

  return TRUE;

error:
  g_propagate_error (error, local_error);
  return FALSE;
}

/*
 * rc_bundle_manager_reinstall_sync:
 * @self: the bundle manager
 * @bundle_id: a bundle
 * @error: used to raise a %RC_ERROR if the operation fails;
 *  in particular, if there is no such app-bundle or if it does not have
 *  a current/active version, %RC_ERROR_NOT_FOUND is raised
 *
 * Reinstall a bundle that was temporarily uninstalled via
 * rc_bundle_manager_uninstall_sync().
 *
 * This function is intended to be thread-safe. Do not interact with
 * global state in this function, except via the filesystem or while
 * holding a lock.
 *
 * Returns: %TRUE on success
 */
gboolean
rc_bundle_manager_reinstall_sync (RcBundleManager *self,
                                  const gchar *bundle_id,
                                  GError **error)
{
  GError *local_error = NULL;
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;
  g_auto (RcBundleVersion) reinstalling_version = RC_BUNDLE_VERSION_INIT;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (bundle_id != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!rc_bundle_manager_open_bundle (self, bundle_id, OPEN_NONE, &bundle,
                                      &local_error) ||
      !rc_bundle_open_role (&bundle, ROLE_UNINSTALLED, &reinstalling_version,
                            &local_error))
    goto error;

  /* FIXME: do we want a way to mark built-in app-bundles as disabled, too? */
  if (bundle.type != CBY_PROCESS_TYPE_STORE_BUNDLE)
    {
      g_set_error (&local_error, RC_ERROR, RC_ERROR_INVALID_ARGUMENT,
                   "\"%s\" is a built-in app-bundle and cannot be uninstalled",
                   bundle_id);
      goto error;
    }

  /* We fail if there is a current version, because reinstalling a
   * previously-uninstalled version over a current version makes very little
   * sense. This can happen if we "uninstall", and then do what appears
   * to be a new, independent install. */
  if (linkat (bundle.fd, ROLE_UNINSTALLED, bundle.fd, ROLE_CURRENT, 0) != 0)
    {
      int saved_errno = errno;
      RcError code = RC_ERROR_FAILED;

      if (saved_errno == EEXIST)
        code = RC_ERROR_EXISTS;
      else if (saved_errno == ENOENT)
        code = RC_ERROR_NOT_FOUND;

      g_set_error (&local_error, RC_ERROR, code,
                   "Unable to link \"%s/%s\" to \"%s\": %s", bundle.path,
                   ROLE_UNINSTALLED, ROLE_CURRENT, g_strerror (saved_errno));
      goto error;
    }

  if (unlinkat (bundle.fd, ROLE_UNINSTALLED, 0) != 0)
    {
      int saved_errno = errno;
      RcError code = RC_ERROR_FAILED;

      if (saved_errno == ENOENT)
        code = RC_ERROR_NOT_FOUND;

      g_set_error (&local_error, RC_ERROR, code,
                   "Unable to delete \"%s/%s\": %s", bundle.path,
                   ROLE_UNINSTALLED, g_strerror (saved_errno));
      goto error;
    }

  if (!rc_bundle_manager_mount_unlocked (self, bundle_id, &bundle,
                                         &reinstalling_version, &local_error))
    goto error;

  if (!rc_bundle_manager_postinst_sync (self, bundle_id, &bundle,
                                        &reinstalling_version, NULL,
                                        &local_error))
    goto error;

  if (!rc_bundle_manager_update_component_index_sync (self, NULL, &local_error))
    goto error;

  return TRUE;

error:
  g_propagate_error (error, local_error);
  return FALSE;
}

/*
 * rc_bundle_manager_mount_app_sync:
 * @self: the bundle manager
 * @bundle_id: an app-bundle
 * @bundle_out: (out caller-allocates) (optional): if not %NULL, used to
 *  return the mounted bundle so that it remains locked against concurrent
 *  access
 * @version_out: (out caller-allocates) (optional): if not %NULL, used to
 *  return the current version
 * @error: used to raise a %RC_ERROR on failure; in particular,
 *  if the specified bundle isn't installed, %RC_ERROR_NOT_FOUND is raised
 *
 * Mount the app-specific static and variable data belonging to @bundle_id
 * in their correct places in %CBY_PATH_PREFIX_STORE_BUNDLE and
 * %CBY_PATH_PREFIX_VARIABLE_DATA.
 */
static gboolean
rc_bundle_manager_mount_app_sync (RcBundleManager *self,
                                  const gchar *bundle_id,
                                  RcBundle *bundle_out,
                                  RcBundleVersion *version_out,
                                  GError **error)
{
  GError *local_error = NULL;
  g_auto (RcBundle) bundle = RC_BUNDLE_INIT;
  g_auto (RcBundleVersion) current_version = RC_BUNDLE_VERSION_INIT;
  g_autofree gchar *built_in_path = NULL;
  gboolean is_built_in;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (bundle_id != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  built_in_path = g_build_filename (CBY_PATH_PREFIX_BUILT_IN_BUNDLE,
                                    bundle_id, NULL);
  is_built_in = g_file_test (built_in_path, G_FILE_TEST_EXISTS);

  /* We're willing to create the per-bundle directory here for built-in
   * apps. For store apps, by definition it should exist. */
  if (!rc_bundle_manager_open_bundle (self, bundle_id,
                                      is_built_in ? OPEN_CREATE : OPEN_NONE,
                                      &bundle, error))
    return FALSE;

  if (rc_bundle_open_role (&bundle, ROLE_CURRENT, &current_version,
                           &local_error))
    {
      /* OK: we already had a current version and will open it */
    }
  else if (g_error_matches (local_error, RC_ERROR, RC_ERROR_NOT_FOUND) &&
           is_built_in)
    {
      /* For built-in app-bundles, we need to be willing to create a per-bundle
       * storage directory for variable data.
       *
       * FIXME: what's the right version number? We guess
       * VERSION_UNKNOWN because we have to use *something*. */
      if (!rc_bundle_open_version (&bundle, VERSION_UNKNOWN, OPEN_CREATE,
                                   &current_version, error))
        return FALSE;

      if (!rc_durable_symlink_at (VERSION_PREFIX VERSION_UNKNOWN, bundle.path,
                                  bundle.fd, ROLE_CURRENT, RC_PATH_FLAGS_NONE,
                                  error))
        return FALSE;
    }
  else
    {
      g_propagate_error (error, local_error);
      return FALSE;
    }

  if (!rc_bundle_manager_mount_unlocked (self, bundle_id, &bundle,
                                         &current_version, error))
    return FALSE;

  if (bundle_out != NULL)
    rc_bundle_steal_from (bundle_out, &bundle);

  if (version_out != NULL)
    rc_bundle_version_steal_from (version_out, &current_version);

  return TRUE;
}

/*
 * Add @bundle_id to the queue of apps that will be mounted during startup.
 * This list is first-in, first-out: to give an app higher priority, add
 * it to the list sooner.
 *
 * If an app-bundle appears in this list more than once, it is mounted at
 * a priority corresponding to its first appearance, and other appearances
 * are ignored.
 *
 * This function must not be called after
 * rc_bundle_manager_mount_startup_apps().
 */
void
rc_bundle_manager_add_startup_app (RcBundleManager *self,
                                   const gchar *bundle_id)
{
  g_return_if_fail (!self->did_startup_apps);
  g_return_if_fail (bundle_id != NULL);
  g_return_if_fail (cby_is_bundle_id (bundle_id));

  g_queue_push_tail (&self->startup_apps, g_strdup (bundle_id));
}

static void
mount_startup_apps_in_thread (GTask *task,
                              gpointer source_object,
                              gpointer task_data,
                              GCancellable *cancellable)
{
  RcBundleManager *self = RC_BUNDLE_MANAGER (source_object);
  const GList *iter;
  gboolean failed = FALSE;
  GError *error = NULL;

  for (iter = task_data; iter != NULL; iter = iter->next)
    {
      const gchar *bundle = iter->data;

      if (!rc_bundle_manager_mount_app_sync (self, bundle, NULL, NULL, &error))
        {
          WARNING ("Unable to mount app \"%s\": %s", bundle, error->message);
          g_clear_error (&error);
          failed = TRUE;
        }
    }

  if (failed)
    g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
                             "Could not mount all startup app-bundles");

  if (!rc_bundle_manager_update_component_index_sync (self, NULL, &error))
    {
      /* Tolerate failure to update the component index: it's non-critical,
       * and it's better that we boot up */
      WARNING ("%s", error->message);
      g_clear_error (&error);
    }

  g_task_return_boolean (task, TRUE);
}

static void
free_string_list (gpointer p)
{
  g_list_free_full (p, g_free);
}

/*
 * Mount all the apps that were queued with
 * rc_bundle_manager_add_startup_app().
 *
 * If any of the app-bundles cannot be mounted, this function will try to
 * mount the rest anyway, and then raise a %RC_ERROR when finished.
 *
 * This function must only be called once.
 */
void
rc_bundle_manager_mount_startup_apps_async (RcBundleManager *self,
                                            GCancellable *cancellable,
                                            GAsyncReadyCallback callback,
                                            gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  GList *startup_apps;

  g_return_if_fail (RC_IS_BUNDLE_MANAGER (self));
  g_return_if_fail (!self->did_startup_apps);
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  self->did_startup_apps = TRUE;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, rc_bundle_manager_mount_startup_apps_async);

  /* takes ownership of the strings */
  startup_apps = g_list_copy (self->startup_apps.head);
  g_queue_clear (&self->startup_apps);
  g_task_set_task_data (task, startup_apps, free_string_list);

  g_task_run_in_thread (task, mount_startup_apps_in_thread);
}

/*
 * Interpret the result of rc_bundle_manager_mount_startup_apps_async().
 */
gboolean
rc_bundle_manager_mount_startup_apps_finish (RcBundleManager *self,
                                             GAsyncResult *result,
                                             GError **error)
{
  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
          rc_bundle_manager_mount_startup_apps_async), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void update_component_index_cb (GObject *object,
                                       GAsyncResult *result,
                                       gpointer user_data);

/* Must be called in the main thread. */
static void
update_component_index (RcBundleManager *self)
{
  g_autoptr (RcObjectList) tasks = NULL;
  g_autoptr (GError) error = NULL;
  GQueue *tasks_for_later = &self->update_component_index.tasks;
  g_autofree gchar *cusb = NULL;

  if (self->update_component_index.subprocess != NULL)
    {
      /* A subprocess is currently running. If necessary, we'll run another
       * when it finishes. */
      DEBUG ("Deferring: we are already running an update task");
      return;
    }

  /* These tasks were scheduled before we started the subprocess. If another
   * is scheduled afterwards, we'll have to call the subprocess again -
   * otherwise it might have missed an update. */
  tasks = g_list_copy (tasks_for_later->head);
  g_queue_clear (tasks_for_later);

  /* Return early if nothing to do */
  if (tasks == NULL)
    {
      DEBUG ("Nothing more to do");
      return;
    }

  cusb = g_find_program_in_path ("canterbury-update-store-bundles");

  /* We run a subprocess (provided by Canterbury) so that we can transition
   * to a more restrictive AppArmor profile, rather than parsing app-supplied
   * data in the highly-privileged Ribchester process. */
  if (cusb != NULL)
    {
      /* New version, does more than update-component-index */
      DEBUG ("Running canterbury-update-store-bundles...");
      self->update_component_index.subprocess =
        g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                          &error,
                          "canterbury-update-store-bundles",
                          NULL);
    }
  else
    {
      /* Older, more limited version */
      DEBUG ("Running canterbury-update-component-index (because "
             "canterbury-update-store-bundles was not found)...");
      self->update_component_index.subprocess =
        g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                          &error,
                          "canterbury-update-component-index",
                          NULL);
    }

  if (self->update_component_index.subprocess == NULL)
    {
      GList *iter;

      for (iter = tasks; iter != NULL; iter = iter->next)
        g_task_return_error (iter->data, g_error_copy (error));

      return;
    }

  g_subprocess_wait_check_async (self->update_component_index.subprocess,
                                 NULL, update_component_index_cb,
                                 g_steal_pointer (&tasks));
}

/* Called in the main thread. */
static void
update_component_index_cb (GObject *object,
                           GAsyncResult *result,
                           gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (RcObjectList) tasks = user_data;
  /* We rely on there being at least one task */
  RcBundleManager *self = g_task_get_source_object (tasks->data);

  if (g_subprocess_wait_check_finish (G_SUBPROCESS (object), result, &error))
    {
      GList *iter;

      DEBUG ("canterbury-update-store-bundles or -update-component-index "
             "finished successfully");

      for (iter = tasks; iter != NULL; iter = iter->next)
        g_task_return_boolean (iter->data, TRUE);
    }
  else
    {
      GList *iter;

      DEBUG ("canterbury-update-store-bundles or -update-component-index "
             "failed: \"%s\"",
             error->message);

      for (iter = tasks; iter != NULL; iter = iter->next)
        g_task_return_error (iter->data, g_error_copy (error));
    }

  g_clear_object (&self->update_component_index.subprocess);

  /* Time has passed; more updates might have been scheduled while we were
   * waiting. If they were, start again.
   * (We are already in the main thread.) */
  if (self->update_component_index.tasks.length > 0)
    {
      DEBUG ("More updates were scheduled: starting again");
      update_component_index (self);
    }
}

/* Called in the main thread.
 * Consumes a reference to @t */
static gboolean
update_component_index_in_main_thread (gpointer t)
{
  g_autoptr (GTask) task = t;
  g_autoptr (GError) error = NULL;
  RcBundleManager *self = g_task_get_source_object (task);
  GQueue *tasks_for_later = &self->update_component_index.tasks;

  if (task != NULL)
    g_queue_push_tail (tasks_for_later, g_object_ref (task));

  update_component_index (self);
  return G_SOURCE_REMOVE;
}

/*
 * Update the cache containing details of all store apps.
 */
void
rc_bundle_manager_update_component_index_async (RcBundleManager *self,
                                                GCancellable *cancellable,
                                                GAsyncReadyCallback callback,
                                                gpointer user_data)
{
  g_autoptr (GTask) task = NULL;

  g_return_if_fail (RC_IS_BUNDLE_MANAGER (self));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, rc_bundle_manager_update_component_index_async);

  DEBUG ("Scheduling update-store-bundles or update-component-index for later");
  g_main_context_invoke (NULL, update_component_index_in_main_thread,
                         g_object_ref (task));
}

/*
 * Interpret the result of rc_bundle_manager_update_component_index_async().
 */
gboolean
rc_bundle_manager_update_component_index_finish (RcBundleManager *self,
                                                 GAsyncResult *result,
                                                 GError **error)
{
  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
          rc_bundle_manager_update_component_index_async), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
store_result (GObject *source_object G_GNUC_UNUSED,
              GAsyncResult *result,
              gpointer user_data)
{
  GAsyncResult **p = user_data;

  g_assert (*p == NULL);
  *p = g_object_ref (result);
}

gboolean
rc_bundle_manager_update_component_index_sync (RcBundleManager *self,
                                               GCancellable *cancellable,
                                               GError **error)
{
  g_autoptr (GMainContext) context = g_main_context_new ();
  g_autoptr (GAsyncResult) result = NULL;

  g_return_val_if_fail (RC_IS_BUNDLE_MANAGER (self), FALSE);
  g_return_val_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable),
                        FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  g_main_context_push_thread_default (context);

  rc_bundle_manager_update_component_index_async (self, cancellable,
                                                  store_result, &result);

  while (result == NULL)
    g_main_context_iteration (context, TRUE);

  g_main_context_pop_thread_default (context);

  return rc_bundle_manager_update_component_index_finish (self, result,
                                                          error);
}

void
rc_bundle_manager_queue_app_bundles (RcBundleManager *bundle_manager)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray) bundles = NULL;
  g_autoptr (CbyComponentIndex) index =
      cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_ONCE, &error);
  guint i;

  if (index == NULL)
    {
      WARNING ("%s", error->message);
      return;
    }

  bundles = cby_component_index_get_built_in_bundles (index);

  for (i = 0; i < bundles->len; i++)
    {
      CbyComponent *component = g_ptr_array_index (bundles, i);
      const gchar *bundle_id;

      bundle_id = cby_component_get_bundle_id (component);
      /* If it's a built-in app bundle then it had better have a bundle ID */
      g_assert (bundle_id != NULL);

      rc_bundle_manager_add_startup_app (bundle_manager, bundle_id);
    }

  g_clear_pointer (&bundles, g_ptr_array_unref);
  bundles = cby_component_index_get_installed_store_bundles (index);

  for (i = 0; i < bundles->len; i++)
    {
      CbyComponent *component = g_ptr_array_index (bundles, i);
      const gchar *bundle_id;

      bundle_id = cby_component_get_bundle_id (component);
      g_assert (bundle_id != NULL);

      rc_bundle_manager_add_startup_app (bundle_manager, bundle_id);
    }
}

/* Created in constructed() */
const gchar *
rc_bundle_manager_get_temp_path (RcBundleManager *self)
{
  static gchar *tmppath = NULL;
  if (tmppath == NULL)
    tmppath = g_build_filename (rc_general_rw_mount_point (), "tmp", NULL);

  return tmppath;
}
