Source: ribchester
Section: libs
Priority: extra
Maintainer: Apertis packagers  <packagers@lists.apertis.org>
Build-Depends:
 autoconf (>= 2.69-9~),
 autoconf-archive,
 autotools-dev,
 libbtrfs-dev,
 debhelper (>= 10.2~),
 dh-apparmor,
 flatpak,
 gawk,
 gettext,
 gobject-introspection,
 hotdoc-0.8 (>= 0.8),
 hotdoc-c-extension-0.8,
 hotdoc-dbus-extension-0.8,
 libcanterbury-0-dev (>= 0.1709.0~),
 libcanterbury-platform-0-dev (>= 0.1709.0~),
 libgirepository1.0-dev,
 libglib2.0-dev (>= 2.48),
 libmount-dev,
 libostree-dev,
 libpolkit-gobject-1-dev,
 libsystemd-dev,
 pkg-config,
 systemd,
Standards-Version: 3.9.2

Package: gir1.2-ribchester-0
Section: introspection
Architecture: any
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: Filesystem volume manager - introspection data
 ribchester manages the btrfs subvolumes used in the Apertis application
 framework.
 .
 This package contains GObject-Introspection data for the
 Ribchester filesystem volume manager.

Package: libribchester5
Section: libs
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Filesystem volume manager - shared library
 ribchester manages the btrfs subvolumes used in the Apertis application
 framework.
 .
 This package contains the shared library for the D-Bus APIs of the
 Ribchester filesystem volume manager.

Package: ribchester
Architecture: any
Depends:
 ribchester-full (= ${binary:Version}),
 ${misc:Depends},
Description: Filesystem volume manager service - transitional package
 ribchester manages the btrfs subvolumes used in the Apertis application
 framework.
 .
 This transitional package arranges for ribchester-full to be installed
 during upgrades.

Package: ribchester-common
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 ribchester (<< ${binary:Version}),
Replaces:
 ribchester (<< ${binary:Version}),
Description: Filesystem volume manager - common files
 ribchester manages the btrfs subvolumes used in the Apertis application
 framework.
 .
 This package contains the files shared by the full and minimal versions
 of Ribchester.

Package: ribchester-core
Architecture: any
Depends:
 canterbury-update-component-index,
 policykit-1,
 ribchester-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 canterbury (<< 0.9.0~),
 ribchester (<< ${binary:Version}),
Replaces:
 ribchester (<< ${binary:Version}),
Description: Filesystem volume manager service - minimal version
 ribchester manages the btrfs subvolumes used in the Apertis application
 framework. It provides maintenance operations (installing, upgrading,
 and removing application bundles), and mounts application
 bundles' subvolumes into their appropriate locations when a program
 from the application bundle is started.

Package: ribchester-full
Architecture: any
Depends:
 btrfs-tools,
 canterbury-update-component-index,
 policykit-1,
 ribchester-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 canterbury (<< 0.9.0~),
 ribchester (<< ${binary:Version}),
Replaces:
 ribchester (<< ${binary:Version}),
Description: Filesystem volume manager service - full version
 ribchester manages the btrfs subvolumes used in the Apertis application
 framework. It provides maintenance operations (installing, upgrading,
 rolling back and removing application bundles), and mounts application
 bundles' subvolumes into their appropriate locations during system boot.
 .
 This version of the ribchester service also manages system data subvolumes
 in /var.

Package: ribchester-dev
Section: libdevel
Architecture: any
Depends:
 gir1.2-ribchester-0 (= ${binary:Version}),
 libcanterbury-0-dev (>= 0.1703.7),
 libcanterbury-platform-0-dev (>= 0.9),
 libglib2.0-dev,
 libmount-dev,
 libostree-dev,
 libpolkit-gobject-1-dev,
 libribchester5 (= ${binary:Version}),
 libsystemd-dev,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ribchester-doc,
Description: Filesystem volume manager - library development files
 ribchester manages the btrfs subvolumes used in the Apertis application
 framework.
 .
 This package provides development files for a library wrapping ribchester's
 D-Bus API.

Package: ribchester-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ribchester (<< ${source:Upstream-Version}.0~),
 ribchester (>= ${source:Version}),
 ${misc:Depends},
Description: Filesystem volume manager - API documentation
 ribchester manages the btrfs subvolumes used in the Apertis application
 framework.
 .
 This package contains API reference documentation.

Package: ribchester-tests
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 canterbury-common (>= 0.1709.0~),
Description: Installed tests for ribchester
 ribchester helps in managing the btrfs subvolumes. It performs create ,snapshot, delete, mount and unmount operation on the subvolumes .
 .
 This package contains the test programs , to test the functionality of the ribchester package.

Package: ribchester-tools
Architecture: any
Multi-Arch: foreign
Depends:
 gir1.2-ribchester-0,
 python3,
 python3-gi,
 ribchester-core (= ${binary:Version}) | ribchester-full (= ${binary:Version}),
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: Command-line tools for Ribchester
 This package contains prototype command-line utilities for the Ribchester
 filesystem volume manager.
